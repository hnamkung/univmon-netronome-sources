#include <stdint.h>
#include <nfp.h>
#include <nfp/mem_atomic.h>

/* Netronome FlowEnv MicroEngine library */
#include <nfp/me.h>

/* Netronome FlowEnv bulk memory library */
#include <nfp/mem_bulk.h>
#include "pkt_meta.h"



/* ethernet (14B) */
struct hdr_ethernet {
    /* dstAddr [32;16] */
    unsigned int dstAddr_0:32;
    /* dstAddr [16;0] */
    unsigned int dstAddr_1:16;
    /* srcAddr [16;32] */
    unsigned int srcAddr_0:16;
    /* srcAddr [32;0] */
    unsigned int srcAddr_1:32;
    unsigned int etherType:16;
};

struct hun_iphdr 
{ 
	unsigned int ihl:4;
	unsigned int version:4;
	uint8_t tos;
	uint16_t tot_len;
	uint16_t id;
	uint16_t frag_off;
	uint8_t ttl;
	uint8_t protocol;
	uint16_t check;
	uint32_t saddr;
	uint32_t daddr;
};

#define ETHERTYPE_IPV4 0x0800

__declspec(cls shared) uint64_t level_a[20] = {0x29f12b1918448bd2, 0x3059ed2a5fe17932, 0x2fbfc44756ef29ce, 0x42b8c7e8714c7290, 0x41c0160d29e7d134, 0x5366fe410ebe882e, 0x708f3243492d0670, 0x5be23b166195507a, 0x50be66790439bb59, 0x47ed329225933422, 0x78a2144658e13d89, 0x5cd346fb32d93859, 0x54a1ecfa52cd3c7e, 0x1d4ceef625dc6781, 0x74a4512a4fccd839, 0x4029dbf47c279114, 0x6d2a879c032d4ac2};
__declspec(cls shared) uint64_t level_b[20] = {0x3c37947064f263f2, 0x4bdbf2cb55211455, 0x6fa1bff864426c09, 0x51f5860c4e33a7dd, 0x3257c26a1f9cd6f8, 0x7ede8b91449d1da9, 0x2b01c89802188937, 0x11d6aba3249e637b, 0x6a3ad2393bbfd2ab, 0x6343b77372c111ee, 0x2b8f1af54032e12a, 0x5302752346587768, 0x203e9eaa6f22bb6f, 0x290f59922ec1954d, 0x1187af1d62f9a1e8, 0x9a5f7ba6f2ad948, 0x1287082f5f2a4f29};

__declspec(cls shared) uint64_t index_a[100] = {0x657fd8a42c682b0b, 0x5b123aec09da8aaa, 0x3b17f77822780597, 0x4ff098d23cc12bfe, 0x3c35b3b869aa13e9, 0x6ebe041c7d000310, 0x6ee6e323784a47b6, 0x62aa2a1011afb30b, 0x239b2c462077b9ed, 0x7f4e5b97715f57cc, 0x44c2f64b5bb7d931, 0x45de090f7424db9e, 0x7477b6a85f790b54, 0x678bf8ad16735af7, 0x7fab243a3cd79d51, 0x2151c6477fb07067, 0x3d3cc99061d52c58, 0x3cc877d50d336020, 0x43e9d63e2cf0a347, 0x421b611c1384972c, 0x71e543fe02d3131d, 0x4653ac311f4da76, 0xec0968085d8b51, 0x112e39dd71d8e4fa, 0x3840fb31322b6ad1, 0x2706657014edb014, 0x6997b5c4671e8d04, 0x51ed1c4125da196c, 0x7303391358984962, 0x784742f10e7c19e8, 0x18d87af42c7035ea, 0x483e79f07db7a09d, 0x4501ab7770a03505, 0x10fae8a648bff2ff, 0x1725a13b277c345c, 0x1a95330539489ae5, 0x2589b3e073804a6, 0x256cf0861323009c, 0x3bd184603849d94e, 0xa0485b32ee5afe8, 0x2e9ff732081e0b88, 0x6dc8ed852261ef12, 0x6e35c8960d042861, 0x5a69a26c4d268ed3, 0x3b81b45440661551, 0x61805b1d31ddfeed, 0x41b0203d1214a67c, 0x5f0b01694b89c53e, 0x796a8d3c425e9c6a, 0x7c81688613f3eb5, 0x4c5cad5358774b4f, 0x2721cb921bbbf250, 0x43dad72654632297, 0x6bc041121af03c02, 0x354a2bf2188b3833, 0x5a0770ec1e86c421, 0x1e946e5e22d9ed01, 0x71ff415b2d1bf7d5, 0x6a5986c41b9ee468, 0x58170d7c51767512, 0x513fad692b91f22b, 0x765be9f40c5ccee1, 0x3a093ef63504a63e, 0x5d72d7471e91daa, 0xd3330e29bcdc8e, 0x27310a3604957354, 0x7ced1b3b3296fe90, 0x4bcbefc939eb9bff, 0x1807a04f1cadaedc, 0x1d2ef49c75bd3cbb, 0x72f4b91b20a4d693, 0x1e82baf518c03a79, 0x7e2789f655d2b62e, 0x71c6d55b34e183b8, 0x1a6c113f2ce04ca6, 0x661f3efc0d6046c5, 0x30f714392d50c34c, 0x2316b10126becaa6, 0x6073b0404b386b38, 0x2eb5706c09e7da69, 0x52b33d8e758e600c, 0x572b7960672dba55, 0x1273fd627d003664, 0x42ecc6e1c6f4be7, 0x20dba08b33091687};
__declspec(cls shared) uint64_t index_b[100] = {0x6ae1f5f318da5b57, 0x6ac5a3f3558edd48, 0x75b70e2e45042a62, 0x2e2f4fdb2423ca8d, 0x1d015c31444ba3d7, 0xbc94f8d4d85b806, 0x54aa3b6c74235ae0, 0x2613a03f527d9ba0, 0x144f874271390878, 0x2a7d4c3a081789a1, 0x3144f022a4f0de2, 0x1fce9da435cf8a4c, 0x3d6e8c4124b9dd4, 0x6f5919a442a29718, 0x6f99500b3a563b6a, 0x18a3bbaf2576eacc, 0x72527795042911d6, 0x27ebdba56da1421c, 0x6a6fa15d46cb1742, 0x65e0d3b60b6b85fb, 0x6f8bd65d45a6ace1, 0x6232822f62010e06, 0x35657721187032ea, 0x56a1137a6905db04, 0x4071d91272624dcb, 0xc2fbc8012007dc, 0x6bc36802e9e10f4, 0x11bb14de115703ea, 0x75f9ee5e6b9099d4, 0x7588d5c56a8ab4cb, 0x7ae3ac70794e2c17, 0x2291e3e6180922c5, 0x25f9150703d3920b, 0x31ea68a9147d48d9, 0x4b51984459e3befd, 0x4db14dc43368a6b5, 0x6eb931fd408111d5, 0x5efd0b9437035244, 0x7859acc0475caefa, 0x697fc06542305cff, 0x6c8711e117a70271, 0x4b9698600beded65, 0xbf6fef404dc4b4f, 0x1e6ae6377c30347a, 0x6e0199de2b1cf63e, 0x658b932e2e6be517, 0x9be202a2131aa65, 0x41f490301f266124, 0x516f153053d82c14, 0x7f2e0ef328d7b4c0, 0x7ff15ce83f0ac4f7, 0x50217069435b2698, 0x387412c94c7d6713, 0x11f3b51f16f80f6e, 0x5c16a8db53a7f118, 0x21b6622f4ce80cf3, 0x1156ee876245e9f5, 0x72bd8164f779007, 0x5ca5760272e6acd2, 0x38001a860ecd7023, 0x7ab1fb653aefe2d1, 0x21121c1e2bf40688, 0x443e4ba355d7dd55, 0x7f9abe5208453af1, 0x2e34040a58f54438, 0x6fc6922555a4f477, 0x5722b9e228d9d11f, 0x1b4ea00e46ee0523, 0x4eb2fc3948cd2a8c, 0x5ade55603ab7403b, 0x26035da326f88bd4, 0x743eeaa04e9de9ff, 0x7cb6b2063e61e7e1, 0x418ec02702e96211, 0x39c83d4e0930e985, 0x2bc6365f621bad6c, 0x1145c9d27cc502d9, 0x37ea6c297f63146c, 0x6507501c431c9215, 0x54b428937fd3f656, 0x5643f01f0649af78, 0x6b24575e1ee41346, 0x18f31f59018f18db, 0x4ee0394662d04b1e, 0x19a6133e66395899};

__declspec(cls shared) uint64_t res_a[100] = {0x27a2b9802c4096d4, 0x1661cdda6f11aeb0, 0x147aab540186224d, 0x29c7018448349689, 0x41f19b485d09b5ca, 0x5f4b9ad05ca0fe9, 0x3d2363b15e6618d2, 0x34744faf401b8510, 0x575b385a25d0c984, 0x414cad2d10fd86d9, 0x2c1886217e0df724, 0x4c809f2d10526a98, 0x2266d2ae0c91af27, 0x406dcad5681f86fe, 0x7356ca1146ec5d3f, 0x22310a4b419ad376, 0x2451fabc02245f45, 0x75fb747e4f9d38b5, 0x3f64125d42f1b82e, 0x402927984de68d10, 0x41a00b8072532228, 0x333fde2e2613b84b, 0x6e1ead2e1ffbdf7d, 0x7f6e1979154a984b, 0x13e185183ae8f4da, 0x5de3fd1722fd2129, 0xb6b1b1524bf058e, 0x67c2008a71b1993d, 0x626364776b58f317, 0x3863b212193fd8aa, 0x7b30d83843c383b7, 0x7fc9bfd61651bc25, 0x371912234ef5d417, 0x2d39a911248ab249, 0x771002253bfd0834, 0x1b98cf714d5b11de, 0x59b9d8083816233d, 0x3b0e0a9316d8572f, 0x15e3c3b01c44630a, 0x75298b4b7a782504, 0x559154d838595ccb, 0x317c880166c51308, 0x1ad4320761207753, 0x40c575486396eb30, 0x7a6a768e638da36b, 0x2d8d60d01ccf0f0d, 0x43a9dde945280eb3, 0x10b392767d54898a, 0x1536000d0c3d605c, 0x699bbe326fe3fac8, 0x5c09547668883e2e, 0x1542e7b45b79e553, 0x3cfa37a04f5a08a6, 0x77bd0a891bfee5c9, 0x36c5899062ca6907, 0x16aa4cf706a2f7c1, 0x5c03063a4a8df907, 0x3290e453467e1f14, 0x65c49ed14f969de, 0x517d116c6a02f33f, 0x5b14259107ae814e, 0x21d8e14325ad0711, 0x4f04237a18b5471d, 0x7920a8744f1395cc, 0x4e51e62862dd7443, 0x40addc8e566b0fce, 0x74305f2c13b87d4c, 0x3493613637d33b31, 0x15a472ab5f6443a6, 0x54d9ff9a341c02fb, 0x7a41f492801f58a, 0x616d8cab5f2e6085, 0xdbe84c656feea36, 0x28254b95288537ca, 0x6a331f7942577767, 0x716bfc6668863cc, 0x6a3e348d19ec4b79, 0x41d1ffca41f04888, 0x8bac31f1d602cb3, 0x34d1c9aa390ed2fd, 0x4f9ef2814c431bfd, 0xfad688241a836d8, 0x59a8d2a95a9e833b, 0x58f3d1406f2eee5f, 0x3ee7d93d6767540e};
__declspec(cls shared) uint64_t res_b[100] = {0x446e4efe260428cb, 0x71e3ddc726f56e3b, 0xd35ee034f9d0dbb, 0x7487216753a22cf5, 0x287e107e6c6cd4f6, 0x14da8cf7185cb6d3, 0xe7b3696bb3b2f5, 0x4ebcfe5159dca039, 0x2e5e0c81208700ef, 0x74a019453adb3cd4, 0x46dc9d233dd0892d, 0x12d4298726f267b9, 0x30822fe33315f8f6, 0x6dd7c3197049d6a5, 0x47ddf7793da24dcb, 0x1bb109a003eef594, 0x21eaac1c35c973a9, 0x5af61be853a24aff, 0xb72f658288b1147, 0x5b373d64079f9f05, 0x2fe7aabb1279f38f, 0x58a857741405ae25, 0x510d97f25372ba70, 0x54546ed873395228, 0x1423326d12c39c6f, 0x188c1cb116afb50e, 0x7905c07a689fc7a8, 0x42c3ae1b3cdb90db, 0x1cbfa2106928d72e, 0x2ea98dd97d9fb97d, 0x58fc86222aca4fd2, 0x50172c9521701347, 0x6d375a804d16c184, 0xdbee95d70cadfb7, 0x6125acb0706c88a3, 0x15f22d7f4e88f81a, 0x3563968f1d31a1ab, 0x5543da6059b6005b, 0x4dc2340508e55c25, 0x65c6686c44f1bea7, 0x72db30531441d40e, 0x1464a6ee5c0b5db7, 0x1a7a1bf646c1c10e, 0x492acf641995d3c3, 0x67e3f8dc27e768cd, 0x5de52d4170fe368f, 0x48dd2d3d30c5252a, 0x3912034e4d971327, 0x11813e4b3c20b6e7, 0x606599db2e582a4e, 0x48aa739f2e8cf0fe, 0x1fbedb102b2007b8, 0x1fedf2fd3eebd06b, 0x7b9ffd7a44ba93fe, 0x56b57d3c25311a9d, 0x3537a34e57c2712d, 0x2ed860ce7fbb8c68, 0x22a7e3433c38908, 0x2957894231f3ddf, 0x67afe3784c7f1076, 0x51ab2bd23cca11e1, 0x2ab006a28a83084, 0x254fcd971a4287a4, 0xecfaca3644f08ed, 0x38fc0d6a2bd4c760, 0xedac9b53df091b1, 0x352a12b15d313dbb, 0xcd32a997b91a962, 0x2f9578377e077b48, 0x1b07c7771bb8637e, 0x189f4b8b02189f4e, 0x5dc2ec94283a18a3, 0x79c2abd85ba0305b, 0x4211c38e1c3dc185, 0x7c602d370e38b5ac, 0x4501aab2706d2c3, 0x684f023a1b1b6356, 0x629fa8a2dff7d2f, 0x1516aad60329d26b, 0x7c3dfd4e49c14b9b, 0x49e2a6283f0ab1fd, 0xaa8c0954cfb7baa, 0x5cb9c4f7283263b0, 0x7223d7ff0f39e0e4, 0x30bf982b729f480d};

#define WIDTH 2048
#define ROW 5

#define SAMPLING_LEVEL 16

__export __mem int32_t sketchArray[174080];
__export __mem int32_t debug[100];

int compute_hash(uint32_t x, int mask, uint64_t aParam, uint64_t bParam)
{
    return ((aParam * x + bParam) >> 32) & mask;
}

int level_hash(int level, uint32_t feature)
{
    return compute_hash(feature, 0x1, level_a[level], level_b[level]);
}

int index_hash(int level, int row, uint32_t feature)
{
    return compute_hash(feature, 0x7ff, index_a[level * ROW + row], index_b[level * ROW + row]);
}

int res_hash(int level, int row, uint32_t feature)
{
    return compute_hash(feature, 0x1, res_a[level * ROW + row], res_b[level * ROW + row]);
}


void sketch(int level, uint32_t feature, uint32_t elem)
{
    __lmem int row;
    __lmem int index;
    __lmem int res;

    __xread int32_t in_xfer;
    __xwrite int32_t out_xfer;

	for(row=0; row<ROW; row++) {
		index = index_hash(level, row, feature);
        index = level*ROW*WIDTH + row*WIDTH + index;

		res = res_hash(level, row, feature);
		res = res*2 - 1;
        res = elem * res;

//        local_csr_write(local_csr_mailbox_0, index);
//        local_csr_write(local_csr_mailbox_1, res);

//        res = 1;
//        index = 10;

        // 1. sync code
        out_xfer = res;
        mem_add32(&out_xfer, &sketchArray[index], sizeof(out_xfer));

        // 2. no sync code
//        mem_read32(&in_xfer, &sketchArray[index], sizeof(in_xfer));
//        res = in_xfer + res;
//        out_xfer = res;
//        mem_write32(&out_xfer, &sketchArray[index], sizeof(out_xfer));

        // 3. no sync code for cls
//        sketchArray[index] = sketchArray[index] + res;

    }
}

__lmem int first = 0;
static void univmon_start(uint32_t key, uint32_t packet_len)
{
    __lmem uint32_t rand;
    __lmem uint32_t rand_10000;
    __lmem uint16_t etherType;
    __lmem uint32_t feature;
    __lmem uint32_t elem;
    __lmem int i;
    __lmem int hash;
/*
    feature = key;
    elem = packet_len;
    */

    if (first == 0) {
        local_csr_write(local_csr_pseudo_random_number,
                        (local_csr_read(local_csr_timestamp_low) & 0xffff) + 1);
        local_csr_read(local_csr_pseudo_random_number);
        first = 1;
    }

    rand = local_csr_read(local_csr_pseudo_random_number);
    rand_10000 = rand % 10000;

    feature = rand_10000;
    elem = 1;
    
    hash = 1;
    for(i=0; i<=SAMPLING_LEVEL; i++) {
        if(i > 0) {
            hash = hash * level_hash(i, feature);
        }

        if(hash == 0)
            break;

        sketch(i, feature, elem);
    }
}

int mc_main(void)
{
    PKT_META_TYPE struct pkt_meta *pmeta = PKT_META_PTR;
    __mem uint8_t *pktdata = (__mem uint8_t *) pmeta->pkt_buf;

    __xread struct hdr_ethernet rd_eth;
    struct hdr_ethernet buf_eth;

    __xread struct hun_iphdr rd_ip;
    struct hun_iphdr buf_ip;

    mem_read8(&rd_eth, pktdata, sizeof(struct hdr_ethernet));
    buf_eth = rd_eth;

    if(buf_eth.etherType == ETHERTYPE_IPV4) {
        mem_read8(&rd_ip, pktdata+14, sizeof(struct hun_iphdr));
        buf_ip = rd_ip;
        univmon_start(buf_ip.daddr, buf_ip.tot_len+14);
//        univmon_start(buf_ip.daddr, buf_ip.tot_len+14);
//        univmon_start(buf_ip.daddr, buf_ip.tot_len+14);
//        univmon_start(buf_ip.daddr, buf_ip.tot_len+14);
    }

    /* packet forwarding */

    if(pmeta->ig_port.type == 0) {
        pmeta->eg_port.type = 1;
        pmeta->eg_port.port = 0;
    }
    else if(pmeta->ig_port.type == 1) {
        pmeta->eg_port.type = 0;
        pmeta->eg_port.port = 0;
    }

    return MC_FORWARD;
}