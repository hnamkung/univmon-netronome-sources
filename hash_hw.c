#include <stdint.h>
#include <nfp.h>
#include <std/hash.h>

/* Netronome FlowEnv MicroEngine library */
#include <nfp/me.h>

/* Netronome FlowEnv bulk memory library */
#include <nfp/mem_bulk.h>
#include "pkt_meta.h"

/**************************************************************************
* Defines *
**************************************************************************/

/* ethernet (14B) */
struct hdr_ethernet {
    /* dstAddr [32;16] */
    unsigned int dstAddr_0:32;
    /* dstAddr [16;0] */
    unsigned int dstAddr_1:16;
    /* srcAddr [16;32] */
    unsigned int srcAddr_0:16;
    /* srcAddr [32;0] */
    unsigned int srcAddr_1:32;
    unsigned int etherType:16;
};

__cls uint32_t mask[10];

__lmem int32_t count;
__lmem int32_t key[50];
__lmem int32_t hash_array[50];

__lmem int32_t first;

int mc_main(void)
{
    PKT_META_TYPE struct pkt_meta *pmeta = PKT_META_PTR;
    __xwrite int32_t out_xfer;
    __xwrite int32_t in_xfer;
    __gpr int32_t hash_val;
    int thread_id;

    mask[0] = 0xffffffff;
    mask[1] = 0xffffffff;
    mask[2] = 0xffffffff;
    mask[3] = 0xffffffff;

    thread_id = __ctx();
    if(thread_id == 4) {
        if(first == 0) {
            cls_hash_init(mask, 4);
            first = 1;
        }

        out_xfer = __ME()*10+thread_id;
        in_xfer = cls_hash(&out_xfer, mask, 4, 0);
        hash_val = in_xfer;
    
        count++;
        if(count >= 50)
            count = 0;
        key[count] = __ME()*10+thread_id;
        hash_array[count] = hash_val;

        /*
        if(thread_id == 4) {
            local_csr_write(local_csr_mailbox0, hash_val);
        }
        else if(thread_id == 5) {
            local_csr_write(local_csr_mailbox1, hash_val);
        }
        else if(thread_id == 6) {
            local_csr_write(local_csr_mailbox2, hash_val);
        }
        else if(thread_id == 7) {
            local_csr_write(local_csr_mailbox3, hash_val);
        }
        */
    }

    if(pmeta->ig_port.type == 0) {
        pmeta->eg_port.type = 1;
        pmeta->eg_port.port = 0;
    }
    else if(pmeta->ig_port.type == 1) {
        pmeta->eg_port.type = 0;
        pmeta->eg_port.port = 0;
    }
    
    return MC_FORWARD;
}