#include <stdint.h>
#include <nfp.h>

/* Netronome FlowEnv MicroEngine library */
#include <nfp/me.h>

/* Netronome FlowEnv bulk memory library */
#include <nfp/mem_bulk.h>
#include "pkt_meta.h"

/**************************************************************************
* Defines *
**************************************************************************/

/* ethernet (14B) */
struct hdr_ethernet {
    /* dstAddr [32;16] */
    unsigned int dstAddr_0:32;
    /* dstAddr [16;0] */
    unsigned int dstAddr_1:16;
    /* srcAddr [16;32] */
    unsigned int srcAddr_0:16;
    /* srcAddr [32;0] */
    unsigned int srcAddr_1:32;
    unsigned int etherType:16;
};

__mem int32_t hun[1000];

__lmem int first = 0;

int mc_main(void)
{
    PKT_META_TYPE struct pkt_meta *pmeta = PKT_META_PTR;
    int step1, step2, step3, step4, step5;
    int i;

    for(i=0; i<100; i++) {
        step1 = (36527972 * i + 22081161);
        step2 = step1 % 39916801;
//        hun[i] += step2;
        local_csr_write(local_csr_mailbox_0, step2);
    }

    if(pmeta->ig_port.type == 0) {
        pmeta->eg_port.type = 1;
        pmeta->eg_port.port = 0;
    }
    else if(pmeta->ig_port.type == 1) {
        pmeta->eg_port.type = 0;
        pmeta->eg_port.port = 0;
    }
    
    return MC_FORWARD;
}