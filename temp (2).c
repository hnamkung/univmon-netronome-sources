#include <nfp.h>

//__declspec(ctm) int old[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
//__declspec(ctm) int new[10];
//__declspec(cls shared) int64_t hash_array[10];
//__declspec(cls shared) int64_t addr_array[10];

__declspec(ctm export) long long int new[16];


int
main(void)
{
    /*
    if (__ctx() == 0) {
        int i, size;
        size = 10;
        for (i = 0; i < size; i++) {
           new[i] = old[size - i - 1];
        }
    }
    */

    __xwrite int64_t out_xfer;
    unsigned int addr;
    SIGNAL my_sig;
    int thread_id;
    thread_id = __ctx();

    out_xfer = 255;
//    hash_array[thread_id] = 15;

    addr = (int32_t)&hash_array[thread_id];
    addr_array[thread_id] = addr;

    __asm {
        cls[write, out_xfer, addr, 0, 1], ctx_swap[my_sig];
//        cls[hash_mask_clear, out_xfer, addr, 0, 2], ctx_swap[my_sig];
//        cls[hash_mask, out_xfer, addr, 0, 2], ctx_swap[my_sig];
    }

    return 0;
}


#include <nfp.h>

//__declspec(ctm export) long long int new[16];

__declspec(cls shared) int new[16];
__declspec(cls shared) int new2[16];

int
main(void)
{
    int i = __ctx();

//    int i = __ctx() + 8 * ((__ME() & 0x0f) - 4);
    __declspec(write_reg) int xfer;

//    volatile __declspec(mem addr40) void * addr;
    volatile __declspec(cls addr32) void * addr;

    unsigned int addr_hi, addr_lo;

    SIGNAL my_signal;

    xfer = i;
    addr = &new[i];
//    addr_hi = ((unsigned long long int)addr >> 8) & 0xff000000;
//    addr_lo = (unsigned long long int)addr & 0xffffffff;

    __asm
    {
        mem[write, xfer, addr, 0, 1], ctx_swap[my_signal];
//        mem[write32, xfer, addr_hi, <<8, addr_lo, 2], ctx_swap[my_signal];
    }
    new2[i] = i;
    return 0;
}

#include <nfp.h>

//__declspec(cls shared) int32_t mask[16] = {0xffffffff};

__declspec(cls shared) int32_t cls_test[10];
__declspec(cls shared) int32_t cls_test2[10];

int
main(void)
{
    int i, j;
    volatile __declspec(cls addr32) void * addr;
    __xwrite int64_t hash_out;
    __xwrite int32_t out;
    __xread int32_t in;
    SIGNAL my_signal;

    i = __ctx();
    if(i == 0) {
        cls_test[0] = 0xffffffff;
        cls_test[1] = 0xffffffff;
        addr = &cls_test[i];

//        out = i;

        hash_out = 10;
        __asm
        {
            cls[hash_mask_clear, hash_out, addr, 0, 2], ctx_swap[my_signal];
//            cls[write_be, out, addr, 0, 1], ctx_swap[my_signal];
//            cls[read_be, in, addr, 0, 1], ctx_swap[my_signal];
        }

//        j = in;
//        cls_test2[i] = j;
    }

    return 0;
}


#include <nfp.h>
#include <nfp_cls.h>

#define NFP_MECL_HASH_INDEX_LO(_h)     (0x40010 + (0x100 * ((_h) & 0x7)))

__declspec(cls shared) int32_t hash_test[10];

int main(void)
{
    int i;
    __declspec(xfer_write_reg) void *hash_pointer;
    __declspec(xfer_write_reg) int64_t hash_out;
    volatile __declspec(cls) void * addr;

    unsigned int addr_hi, addr_lo;

    SIGNAL my_signal;

    if(__ctx() == 0) {
        hash_test[0] = 0xffffffff;
        hash_test[1] = 0xffffffff;

        hash_test[2] = (int32_t)(&hash_test[0]) & 0xffff;

        addr = &hash_test[2];

        hash_out = 10;
        hash_pointer = &hash_out;

//        addr_hi = ((unsigned long long int)(__addr40)hash_pointer >> 8) & 0xff000000;
//        addr_lo = (unsigned long long int)(__addr40)hash_pointer & 0xffffffff;

//        local_csr_write(local_csr_mailbox_0, addr_hi);
//        local_csr_write(local_csr_mailbox_1, addr_hi);

        
        cls_hash_mask_ptr32(hash_pointer, addr, 2, ctx_swap, &my_signal);

//        __asm
//        {
//            cls[hash_mask_clear, hash_out, addr, 0, 2], ctx_swap[my_signal];
//            cls[hash_mask, hash_out, addr, 0, 2], ctx_swap[my_signal];
//        }
    }

    return 0;
}