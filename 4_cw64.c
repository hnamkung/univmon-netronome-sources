#include <stdint.h>
#include <nfp.h>
#include <nfp/mem_atomic.h>

/* Netronome FlowEnv MicroEngine library */
#include <nfp/me.h>

/* Netronome FlowEnv bulk memory library */
#include <nfp/mem_bulk.h>
#include "pkt_meta.h"

#define DIMENSION 3

/* ethernet (14B) */
struct hdr_ethernet {
    /* dstAddr [32;16] */
    unsigned int dstAddr_0:32;
    /* dstAddr [16;0] */
    unsigned int dstAddr_1:16;
    /* srcAddr [16;32] */
    unsigned int srcAddr_0:16;
    /* srcAddr [32;0] */
    unsigned int srcAddr_1:32;
    unsigned int etherType:16;
};

struct hun_iphdr 
{ 
	unsigned int ihl:4;
	unsigned int version:4;
	uint8_t tos;
	uint16_t tot_len;
	uint16_t id;
	uint16_t frag_off;
	uint8_t ttl;
	uint8_t protocol;
	uint16_t check;
	uint32_t saddr;
	uint32_t daddr;
};

#define ETHERTYPE_IPV4 0x0800

#define WIDTH 1024
#define ROW 5

#define SAMPLING_LEVEL 16

#define LowOnes ((((uint64_t)1) << 32) - 1)
#define LOW64(x) (x & LowOnes)
#define HIGH64(x) (x >> 32)

__export __emem int32_t sketchArray[DIMENSION*WIDTH*ROW*(SAMPLING_LEVEL+1)];

__export __mem uint64_t debug[2000];

__lmem uint64_t p0 = (((uint64_t)1) << 32) - 1;
__lmem uint64_t p1 = (((uint64_t)1) << 32) - 1;
__lmem uint64_t p2 = (((uint64_t)1) << 25) - 1;
__lmem uint64_t p3 = (((uint64_t)1) << 57) - 1;

__lmem uint32_t global_r0, global_r1, global_r2;

__declspec(cls shared) uint64_t level_param[(SAMPLING_LEVEL+1) * 15] = {0x169c109c, 0x60067d5c, 0x4a105444, 0x760c6258, 0x270cbbf4, 0x3507a433, 0x8ad2b78, 0x2100d7bb, 0x3e5346ea, 0x4d54cc9d, 0x7a457f14, 0x689a38c2, 0x65047e34, 0x9f9bbbc, 0x6a9438c1, 0x21537326, 0x6dacd4e1, 0x6dcc3807, 0x7a728be1, 0x763d9f94, 0x47b7ea31, 0x7b705bbf, 0xd9797e9, 0x5b7648f7, 0x32b07f0a, 0x61648385, 0xdf6c2b7, 0x4369838a, 0x443afb99, 0x7c5c1ccd, 0x7aae9506, 0x5bb9fbd6, 0xf44cda6, 0x6e19551e, 0x41235d0a, 0x78b15cee, 0x444c4b29, 0x6cda93c6, 0x21be3ff, 0x75157b6d, 0x555a6d28, 0x23b488df, 0x2185fac9, 0x6313b350, 0x22617c01, 0x45956e1e, 0x55709742, 0x52da99e0, 0xdac6b9f, 0x32cd9abc, 0x5869c6b2, 0x873fd77, 0x770593f6, 0xf353886, 0x6f11ed36, 0x1ecad32, 0x595eae9a, 0x57192e4c, 0x362eac40, 0x322eb98a, 0x254d7d8b, 0x26d43cf, 0x537ecc47, 0x29957424, 0x12fbfcd0, 0x5d92c56c, 0x4edf6572, 0x2c7d47d2, 0x54f642cf, 0x739c4f9c, 0x1b2ec610, 0x1bcd5261, 0x44db6c89, 0x21b2b9b0, 0x5cbce318, 0x7ba15796, 0x1d7d7843, 0x186285d5, 0x6c406f74, 0x7a556031, 0x7b1e6fb5, 0xb381339, 0xe7608f0, 0x6744cbfa, 0x55abbf0d, 0x7e8c1a6b, 0x201aa7b5, 0x35f86c8a, 0x489df3b4, 0x79ead3aa, 0x108ba966, 0x411e7006, 0x354cbb50, 0x409d9c86, 0x2b8f428e, 0x4a5a8ef9, 0x7f60a391, 0x2192c5ec, 0x2b001a2c, 0x13b654c2, 0x277696aa, 0x5a9d8723, 0x1c13394f, 0x32177fef, 0x24d03d9a, 0x1fe8c8b3, 0x6bd07022, 0x4171ff7a, 0x233bc427, 0x28caee83, 0x22f0ef61, 0x77f3db32, 0x2abbed24, 0x17cdec67, 0x505b6a66, 0x21a677c1, 0x38003129, 0x149b9778, 0x72f551d9, 0x47d0b885, 0x2c3ed1a5, 0x5035be54, 0x7862a5ef, 0x147c2ba8, 0x64162d19, 0x6beaf7a4, 0xf286756, 0x259846e0, 0x30552f68, 0x289b6da2, 0x743abb81, 0x3bec4dc4, 0x16e59d98, 0x3ccd71e6, 0x53f0d239, 0x20585720, 0x7c00677, 0x4fe8729a, 0x25bc0572, 0x5b0992b8, 0x517f9cb9, 0x110a5f7c, 0x4002cea1, 0x784bccdb, 0x38767b90, 0x6aaa47e5, 0x51564218, 0x760d5f5f, 0x67f09285, 0x63238f12, 0x57e8bac6, 0x703e4a40, 0x97ce952, 0x67ba0b5b, 0x673fb890, 0x86e2ae5, 0x78c224b5, 0x11f82503, 0x3a45f12c, 0x45dc9d97, 0x16ee4856, 0x75cf11dc, 0x6d9dbef0, 0xf62b6c9, 0x19ce4e03, 0xb19109a,
0x1a90f427, 0x228d3a11, 0x65df43cf, 0x24dcff49, 0x28fc2487, 0x41c23016, 0x33e31614, 0x1be95a9, 0x7538b24, 0x7dd3ec3d, 0x643ad954, 0x53924d34, 0x2a0abbc9, 0x26ae9eaf, 0x1ffae193, 0xfef8a4c, 0x37608bc0, 0x237706a7, 0x5b55d021, 0x5ed3555f, 0x48a009c, 0x2e0418, 0x4d0ec3bf, 0x455581f, 0x7f0c5d71, 0x4cae6e1, 0x24fbc03c, 0xc09b61c, 0x5192ee70, 0x2fe8e6b0, 0x63814562, 0x3bf257f7, 0x236d37e0, 0x576f654b, 0x52634bc5, 0x75039cc4, 0x402943e0, 0x55274808, 0x5ea30e5, 0x52300d6b, 0x50c115f4, 0x34886f97, 0x69563d72, 0x1cdc3d65, 0x257813a0, 0x6e508097, 0x672a7215, 0x13a7f59d, 0x77ee207f, 0x3297af5c, 0x479daf7, 0x5c16906c, 0x4d63d1af, 0x545b5fda, 0x3af20c7a, 0x69113bd1, 0x6a6e4c3a, 0x6f4eaa6c, 0x1596d18b, 0x6c5a0fee, 0xccc09d5, 0x27998583, 0x500c6fc4, 0x607dd5ea, 0x65672723, 0x573a9ed7, 0x499261fe, 0x295f906e, 0x41033afa, 0x3b15146e, 0x64f0600f, 0x6132ac8e, 0x45ded07c, 0x2749a0ba, 0x3feb6994, 0x78609454, 0xcaa548a, 0x2943685, 0x50875715, 0x65664d00, 0x1f496f02, 0xe16fc5a, 0x7117ff0, 0xde66930, 0x1203d571, 0x39b1f6f4, 0x52cc2ac3, 0x5a0b92ac, 0x2dca8863};

__declspec(cls shared) uint64_t index_param[ROW * 15] = {0x54437269, 0x180c76b7, 0x5a45f4b6, 0x26c50707, 0x51507073, 0x7806b7b9, 0x108103f, 0x386298a0, 0x51124d4b, 0x890a082, 0x4f19bb32, 0x2250f830, 0x71d628e9, 0x23181362, 0x1a098ee, 0x44795d86, 0x7beb2b88, 0xc772d47, 0x643d96b5, 0x771787d, 0x2a9d5a5c, 0x409763df, 0x131de69a, 0x100e9244, 0x2ca4b698, 0x71cfcc0d, 0x15da3db, 0x2ab07f90, 0x2986dcd5, 0x550c393f, 0x4b71a96a, 0x132994d8, 0xeeafabc, 0x64f3544a, 0x2322fa0d, 0x4d4e7d80, 0x5c118626, 0x27e5803, 0x34c42e3c, 0x3bb78034, 0xc41f48d, 0x3e1e5b44, 0x3af9ed38, 0x6e4515c7, 0x7998f45f, 0x61c8f09b, 0x4f2c8244, 0x731c66f7, 0x4dac1d2b, 0x5eaf19e3, 0x39ccb6a5, 0x2eeb2848, 0x50a1a708, 0x24dd2793, 0x335136cd, 0x18e4e70d, 0x5bfd133f, 0x43fabc47, 0x25efc2f, 0x29fe72e0, 0x7a3717fa, 0x37075cc5, 0x445ba5bc, 0x5cddfcb3, 0x71f97b66, 0x32149fff, 0x68143808, 0x76aca9a, 0x741b4843, 0x2b256242, 0x2355f12f, 0x5f4d56c8, 0x4e7c9559, 0x55312e50, 0xfd7b1e2};
__declspec(cls shared) uint64_t res_param[ROW * 15] = {0xba1682f, 0x11beeba0, 0xd5f5e7a, 0x7033a271, 0x4dede243, 0x3ca1c4ad, 0x207758f4, 0x7b710bd2, 0x3abf474e, 0x63e27003, 0x27280830, 0x35319b65, 0x4bd11f2b, 0xe5566f0, 0x8d91dea, 0x54a2188c, 0x5df9babc, 0x3652bcd7, 0x71ebe31d, 0x358bbf59, 0x69bf8785, 0x1d5a6300, 0x1919a40e, 0x6261a001, 0x774fd41c, 0x19eeb776, 0x74eb147, 0x3f58b010, 0x578f0aed, 0x70167a83, 0x5eb2bcae, 0x28817414, 0x4aebe1d2, 0x4436c86b, 0x68a00bc9, 0x6365eec8, 0x391fbf73, 0x53523451, 0x39e8dc93, 0x66e95498, 0x60b0fbf0, 0x36c7b28, 0x470980d9, 0x40ea4afe, 0x55e18afd, 0x648bff9b, 0x233a4caf, 0x4880873a, 0x6e2e1605, 0x13a7dac6, 0x710c073e, 0x4cafb16d, 0x22a99270, 0x2acd08d7, 0x7aff7134, 0x18614c02, 0x1bc523cf, 0x2bb1f947, 0x3164baba, 0x4c1f22ab, 0x6badcc32, 0x5f391dd8, 0x1ed680bf, 0x149e8b6a, 0x34d2e6b9, 0x22996c7, 0x7869e3ec, 0x6ff2d8b6, 0x3071ce25, 0x7f94f7fb, 0xd24c12d, 0x68067818, 0xb6a503, 0x570b6852, 0x2dee0c23};


// cw64
__forceinline uint64_t Mod64Prime89() {
	uint64_t r0, r1, r2;
	r2 = global_r2;
	r1 = global_r1;
	r0 = global_r0 + (r2>>25);
	r2 &= p2;
	return (r2 == p2 && r1 == p1 && r0 >= p0) ? (r0 - p0) : (r0 + (r1<<32));
}

__forceinline void MultAddPrime89(uint64_t x, uint32_t a0, uint32_t a1, uint32_t a2, uint32_t b0, uint32_t b1, uint32_t b2)
{
	uint64_t x1, x0, c21, c20, c11, c10, c01, c00;
	uint64_t d0, d1, d2, d3, s0, s1, carry;

	x1 = HIGH64(x);
	x0 = LOW64(x);
	
	c21 = a2*x1;
	c20 = a2*x0;
	
	c11 = a1*x1;
	c10 = a1*x0;
	
	c01 = a0*x1;
	c00 = a0*x0;
	
	d0 = (c20>>25)+(c11>>25)+(c10>>57)+(c01>>57);
	d1 = (c21<<7);
	d2 = (c10&p3) + (c01&p3);
	d3 = (c20&p2) + (c11&p2) + (c21>>57);
	
	s0 = b0 + LOW64(c00) + LOW64(d0) + LOW64(d1);
	global_r0 = LOW64(s0); carry = HIGH64(s0);
	
	s1 = b1 + HIGH64(c00) + HIGH64(d0) + HIGH64(d1) + LOW64(d2) + carry;
	global_r1 = LOW64(s1);
	
	carry = HIGH64(s1);
	global_r2 = b2 + HIGH64(d2) + d3 + carry;
}

__forceinline int cw64_hash(int type, int level, int row, uint64_t x)
{
    uint64_t ret;
    int ret_int;
    uint32_t a0, a1, a2;
    uint32_t b0, b1, b2;
    uint32_t c0, c1, c2;
    uint32_t d0, d1, d2;
    uint32_t e0, e1, e2;

    a0 = a1 = a2 = 0;
    b0 = b1 = b2 = 0;
    c0 = c1 = c2 = 0;
    d0 = d1 = d2 = 0;
    e0 = e1 = e2 = 0;

    // set A~E
    if(type == 0) { // level hash
        a0 = level_param[(level * 15)];
        a1 = level_param[(level * 15) + 1];
        a2 = level_param[(level * 15) + 2];

        b0 = level_param[(level * 15) + 3];
        b1 = level_param[(level * 15) + 4];
        b2 = level_param[(level * 15) + 5];

        c0 = level_param[(level * 15) + 6];
        c1 = level_param[(level * 15) + 7];
        c2 = level_param[(level * 15) + 8];

        d0 = level_param[(level * 15) + 9];
        d1 = level_param[(level * 15) + 10];
        d2 = level_param[(level * 15) + 11];

        e0 = level_param[(level * 15) + 12];
        e1 = level_param[(level * 15) + 13];
        e2 = level_param[(level * 15) + 14];
    }
    else if(type == 1) { // index hash
        a0 = index_param[(row * 15)];
        a1 = index_param[(row * 15) + 1];
        a2 = index_param[(row * 15) + 2];

        b0 = index_param[(row * 15) + 3];
        b1 = index_param[(row * 15) + 4];
        b2 = index_param[(row * 15) + 5];

        c0 = index_param[(row * 15) + 6];
        c1 = index_param[(row * 15) + 7];
        c2 = index_param[(row * 15) + 8];

        d0 = index_param[(row * 15) + 9];
        d1 = index_param[(row * 15) + 10];
        d2 = index_param[(row * 15) + 11];

        e0 = index_param[(row * 15) + 12];
        e1 = index_param[(row * 15) + 13];
        e2 = index_param[(row * 15) + 14];
    }

    else if(type == 2) { // res hash
        a0 = res_param[(row * 15)];
        a1 = res_param[(row * 15) + 1];
        a2 = res_param[(row * 15) + 2];

        b0 = res_param[(row * 15) + 3];
        b1 = res_param[(row * 15) + 4];
        b2 = res_param[(row * 15) + 5];

        c0 = res_param[(row * 15) + 6];
        c1 = res_param[(row * 15) + 7];
        c2 = res_param[(row * 15) + 8];

        d0 = res_param[(row * 15) + 9];
        d1 = res_param[(row * 15) + 10];
        d2 = res_param[(row * 15) + 11];

        e0 = res_param[(row * 15) + 12];
        e1 = res_param[(row * 15) + 13];
        e2 = res_param[(row * 15) + 14];
    }

	global_r0 = 0;
	global_r1 = 0;
	global_r2 = 0;

	MultAddPrime89(x, a0, a1, a2, b0, b1, b2); // (r,x,A,B)
	MultAddPrime89(x, global_r0, global_r1, global_r2, c0, c1, c2); // (r,x,r,C)
	MultAddPrime89(x, global_r0, global_r1, global_r2, d0, d1, d2); // (r,x,r,D)
	MultAddPrime89(x, global_r0, global_r1, global_r2, e0, e1, e2); // (r,x,r,E)

    ret = Mod64Prime89();
    ret_int = ret & 65535;
	return ret_int;
}

int level_hash(int level, uint64_t x)
{
    int ret = cw64_hash(0, level, 0, x);
    ret = ret & 1;
    return ret;
}

int index_hash(int level, int row, uint64_t x)
{
    int ret = cw64_hash(1, level, row, x);
    ret = ret & 1023;
    return ret;
}

int res_hash(int level, int row, uint64_t x)
{
    int ret = cw64_hash(2, level, row, x);
    ret = ret & 1;
    return ret;
}

void sketch(int dimension, int level, uint64_t feature, uint32_t elem)
{
    __lmem int row;
    __lmem int index;
    __lmem int res;

    __xread int32_t in_xfer;
    __xwrite int32_t out_xfer;

	for(row=0; row<ROW; row++) {
		index = index_hash(level, row, feature);
        index = level*ROW*WIDTH + row*WIDTH + index;
        index = dimension*WIDTH*ROW*(SAMPLING_LEVEL+1) + index;


		res = res_hash(level, row, feature);
		res = res*2 - 1;
        res = elem * res;

        // 1. sync code
        out_xfer = res;
        mem_add32(&out_xfer, &sketchArray[index], sizeof(out_xfer));
    }
}

__lmem int first = 0;
static void univmon_start(int dimension, uint32_t src, uint32_t dst, uint32_t packet_len)
{
    __lmem uint32_t rand;
    __lmem uint16_t etherType;
    __lmem uint32_t elem;
    __lmem int i;
    __lmem int hash;
    __xwrite int32_t out_xfer = 1;
    __lmem uint64_t feature;

    if (first == 0) {
        local_csr_write(local_csr_pseudo_random_number,
                        (local_csr_read(local_csr_timestamp_low) & 0xffff) + 1);
        local_csr_read(local_csr_pseudo_random_number);
        first = 1;
    }

    rand = local_csr_read(local_csr_pseudo_random_number);
    feature = rand;
    rand = local_csr_read(local_csr_pseudo_random_number);
    feature = (feature << 32) | rand;

//    feature = (((uint64_t)src) << 32) | dst;
    elem = 1;
//    elem = packet_len;

    hash = 1;
    for(i=0; i<=SAMPLING_LEVEL; i++) {
        if(i > 0) {
            hash = hash * level_hash(i, feature);
        }

        if(hash == 0) {
//            mem_add32(&out_xfer, &debug[i], sizeof(out_xfer));
            break;
        }

        sketch(dimension, i, feature, elem);
    }
}

int mc_main(void)
{
    int i;
    PKT_META_TYPE struct pkt_meta *pmeta = PKT_META_PTR;
    __mem uint8_t *pktdata = (__mem uint8_t *) pmeta->pkt_buf;

    __xread struct hdr_ethernet rd_eth;
    struct hdr_ethernet buf_eth;

    __xread struct hun_iphdr rd_ip;
    struct hun_iphdr buf_ip;

    mem_read8(&rd_eth, pktdata, sizeof(struct hdr_ethernet));
    buf_eth = rd_eth;

    if(buf_eth.etherType == ETHERTYPE_IPV4) {
        mem_read8(&rd_ip, pktdata+14, sizeof(struct hun_iphdr));
        buf_ip = rd_ip;

        for(i=0; i<DIMENSION; i++) {
            univmon_start(i, buf_ip.saddr, buf_ip.daddr, buf_ip.tot_len+14);
        }
    }

    /* packet forwarding */

    if(pmeta->ig_port.type == 0) {
        pmeta->eg_port.type = 1;
        pmeta->eg_port.port = 0;
    }
    else if(pmeta->ig_port.type == 1) {
        pmeta->eg_port.type = 0;
        pmeta->eg_port.port = 0;
    }

    return MC_FORWARD;
}