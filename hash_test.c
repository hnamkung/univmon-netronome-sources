#include <nfp.h>
#include <nfp_cls.h>
#include <std/hash.h>

#define NFP_MECL_HASH_INDEX_LO(_h)     (0x40010 + (0x100 * ((_h) & 0x7)))

__declspec(cls shared) int32_t hash_test[10];

int main(void)
{
    int i;
    __declspec(xfer_write_reg) void *hash_pointer;
    __declspec(xfer_write_reg) int64_t hash_out;
    volatile __declspec(cls) void * addr;

    SIGNAL my_signal;

    if(__ctx() == 0) {
        hash_test[0] = 0xffffffff;
        hash_test[1] = 0xffffffff;

        hash_test[2] = (int32_t)(&hash_test[0]) & 0xffff;

        addr = &hash_test[2];

        hash_out = 10;
        hash_pointer = &hash_out;

        cls_hash_mask_ptr32(hash_pointer, addr, 2, ctx_swap, &my_signal);
    }

    return 0;
}