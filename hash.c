#include <stdint.h>
#include <nfp.h>

#include <std/hash.h>

/* Netronome FlowEnv MicroEngine library */
#include <nfp/me.h>

/* Netronome FlowEnv bulk memory library */
#include <nfp/mem_bulk.h>
#include "pkt_meta.h"

/**************************************************************************
* Defines *
**************************************************************************/

/* ethernet (14B) */
struct hdr_ethernet {
    /* dstAddr [32;16] */
    unsigned int dstAddr_0:32;
    /* dstAddr [16;0] */
    unsigned int dstAddr_1:16;
    /* srcAddr [16;32] */
    unsigned int srcAddr_0:16;
    /* srcAddr [32;0] */
    unsigned int srcAddr_1:32;
    unsigned int etherType:16;
};

__cls uint32_t mask[10];
__mem __shared int32_t me_array[10];
__mem __shared int32_t hash_array[10];

int mc_main(void)
{
    PKT_META_TYPE struct pkt_meta *pmeta = PKT_META_PTR;
    __xwrite int32_t out_xfer;
    __xwrite int32_t in_xfer;
    __gpr int32_t hash_val;
    int thread_id;

    mask[0] = 0xffffffff;
    mask[1] = 0xffffffff;
    mask[2] = 0xffffffff;
    mask[3] = 0xffffffff;

    cls_hash_init(mask, 4);

    thread_id = __ctx();
    out_xfer = __ME()+10;

    me_array[thread_id] = __ME()+10;

    in_xfer = cls_hash(&out_xfer, mask, 4, thread_id);
    hash_val = in_xfer;

    hash_array[thread_id] = hash_val;
    if(thread_id == 0) {
        local_csr_write(local_csr_mailbox0, hash_val);
    }
    else if(thread_id == 1) {
        local_csr_write(local_csr_mailbox1, hash_val);
    }
    else if(thread_id == 2) {
        local_csr_write(local_csr_mailbox2, hash_val);
    }
    else if(thread_id == 3) {
        local_csr_write(local_csr_mailbox3, hash_val);
    }

    /* reflect back */
    pmeta->eg_port.type = pmeta->ig_port.type;
    pmeta->eg_port.port = pmeta->ig_port.port;
    
    return MC_FORWARD;
}