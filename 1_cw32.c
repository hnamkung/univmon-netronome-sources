#include <stdint.h>
#include <nfp.h>
#include <nfp/mem_atomic.h>

/* Netronome FlowEnv MicroEngine library */
#include <nfp/me.h>

/* Netronome FlowEnv bulk memory library */
#include <nfp/mem_bulk.h>
#include "pkt_meta.h"

#define DIMENSION 3

/* ethernet (14B) */
struct hdr_ethernet {
    /* dstAddr [32;16] */
    unsigned int dstAddr_0:32;
    /* dstAddr [16;0] */
    unsigned int dstAddr_1:16;
    /* srcAddr [16;32] */
    unsigned int srcAddr_0:16;
    /* srcAddr [32;0] */
    unsigned int srcAddr_1:32;
    unsigned int etherType:16;
};

struct hun_iphdr 
{ 
	unsigned int ihl:4;
	unsigned int version:4;
	uint8_t tos;
	uint16_t tot_len;
	uint16_t id;
	uint16_t frag_off;
	uint8_t ttl;
	uint8_t protocol;
	uint16_t check;
	uint32_t saddr;
	uint32_t daddr;
};

#define ETHERTYPE_IPV4 0x0800

#define WIDTH 1024
#define ROW 5

#define SAMPLING_LEVEL 16

#define LowOnes ((((uint64_t)1) << 32) - 1)
#define LOW64(x) (x & LowOnes)
#define HIGH64(x) (x >> 32)

__export __emem int32_t sketchArray[DIMENSION*WIDTH*ROW*(SAMPLING_LEVEL+1)];

__export __mem uint64_t debug[2000];

uint64_t p = (((uint64_t)1) << 61) - 1;

__declspec(cls shared) uint64_t level_param[(SAMPLING_LEVEL+1) * 5] = {0x4c4ca1863b008b8c, 0x20c9b29719ebe852, 0x4ee16cc931ad2894, 0x5f432606317584c6, 0x1a5ffc8813bc523f, 0x16edf0775f462063, 0x74f8747263a5245c, 0x6ef6471f6ab77d22, 0x347879ea518c0c8f, 0x418caa1c79f435e2, 0x131f0cd05b82357e, 0x4186122149105622, 0x4f86f7a725ed1dbb, 0x723af3707c4478a6, 0x734d1a064ae7bb0d, 0x6aa24def45aabc9a, 0x4c444631165c6215, 0xb2b5c2b4bb010c7, 0x141d9fa324db90a6, 0x4af595314115ea69, 0x7c4083de6f91a38c, 0x4288758d0cdd9e1b, 0x29b705362eb7319f, 0x7d1ad5ae6cc2d2ac, 0x6a9551fc7139ae0e, 0x7909a1b96758b6c3, 0x7146f13668864a53, 0x4879c4c132648013, 0x6414793d7c23c71f, 0x10e603e56d09b80d, 0x2c7fc1be7788b7c4, 0x30d8e22b6ee88e1a, 0x65c985d811725c1d, 0x6afd7cdd2810c80b, 0x65bd5eb97395f7dd, 0x3695126974ebc77d, 0x23761b831f0847a5, 0x5897b18d4f09cd6b, 0xc8d5457129c2231, 0x4888c8820c23f402, 0x2b1277474954ec68, 0x666cc5746f177734, 0x6d932fe65a2ddd3d, 0x7917fe0b1627c249, 0xc4451fc59627dae, 0x3270c7770a376f80, 0x3d7941bd63cafed2, 0x241fc02d2081dce2, 0x2dcc8a1a4f7ec873, 0xc9a1ecb5a5ba7e3, 0x166e39191e66a3d0, 0x648cbc47539d0ce3, 0x6bb935f74c862261, 0x7a3b358645372f1b, 0x29f5b91d434c3470, 0x52eace40358af647, 0x362ecac73a02ea9b, 0x157883de1b196ed5, 0x22bb6fd93fac215f, 0x3dc304a14e6505b4, 0x7314418536d9bdc9, 0x113af3413538393d, 0x7e35ef1606e3ce16, 0x55ef09e240720081, 0x7c7f363403bfcfc7, 0x599c4e4724d74547, 0x31054b36528cdf5e, 0x1e9cbea944a958f2, 0x4a0e9b1504e7a5af, 0x42e86ad0a8bca00, 0x523303331317336e, 0x5831848c1af53090, 0x57474bc309bf1bf9, 0x61c57d6e6cadf2e7, 0x1026536f442fa4b1, 0x75ecb506075f4f7, 0x6025b59a4bb5c2c6, 0xa067bff2fb6a77d, 0xae180425aaaef5, 0x68c46a2437149738, 0x2f2716702f322b3f, 0x6b54d4e68e63f52, 0x6447564b0772895c, 0x7097fad509d4efb3, 0x7ac4d5cf10b14801};
__declspec(cls shared) uint64_t index_param[ROW * 5] = {0x72e68c777e10138f, 0x517451f12bafcefe, 0x233e9c1a637a8109, 0x7fada8e4622b5238, 0xa230ce20322cea1, 0x28c723ad29f747c5, 0x2988ec095c40582c, 0x45ce0056d776a7f, 0x3af0effe201c2aed, 0x19463e1352955e5b, 0x6f0215d571e78be2, 0x1890dcda4e9378cf, 0x33e08656599392b5, 0x6784d70442407cb4, 0x17bb2f6702251f5d, 0x7d91f0d8785b354f, 0x2c09134407cfebf2, 0x638b60df53850387, 0x41aabee730db515f, 0xebb4d082abe4fc6, 0x5472bab93840f2ff, 0x3011603344c2ccec, 0x511bbd387423d121, 0x5b77091863f63591, 0x3936f5dc47475bdc};
__declspec(cls shared) uint64_t res_param[ROW * 5] = {0x167cd5f55dc2d35b, 0x21c22b7452b2ddfc, 0x5d0bf5ce3c36d71b, 0x34644c7f24da40b8, 0x74cf02ee42c9932b, 0x3e117e7e6e87a207, 0xa9eb24238c91080, 0x185a60a0557db4dd, 0x2fed4504024e2231, 0x57a6c0250490ea18, 0x7bdeaa775789aa29, 0xf0296a572f03255, 0x7778a56611b308d1, 0x107da6b2a94a754, 0x57591a369ac6721, 0x5406cca50a658fbc, 0x11bf84f934b30089, 0x53e83d6634171e93, 0x59d85f9c0c6d28d9, 0x5294c4ee250d0f9d, 0x645f2eb424f35ee7, 0x65dba2a43692f13a, 0x691b32d378a5f08d, 0x5653f6dc227411cb, 0x723c38184f86ea3f};

__forceinline uint64_t MultAddPrime32(uint32_t x, uint64_t a, uint64_t b)
{
	uint64_t a0, a1, c0, c1, c;
	a0 = LOW64(a) * x;
	a1 = HIGH64(a) * x;
	c0 = a0 + (a1 << 32);
	c1 = (a0>>32) + a1;
	c = (c0 & p) + (c1 >> 29) + b;
	return c;
}

__forceinline int cw32_hash(int type, int level, int row, uint32_t x)
{
    uint64_t a,b,c,d,e;
	uint64_t h;

    a = b = c = d = e = 0;

    if(type == 0) { // level hash
        a = level_param[(level*5)];
        b = level_param[(level*5)+1];
        c = level_param[(level*5)+2];
        d = level_param[(level*5)+3];
        e = level_param[(level*5)+4];
    }

    if(type == 1) { // index hash
        a = index_param[(row*5)];
        b = index_param[(row*5)+1];
        c = index_param[(row*5)+2];
        d = index_param[(row*5)+3];
        e = index_param[(row*5)+4];
    }

    if(type == 2) { // res hash
        a = res_param[(row*5)];
        b = res_param[(row*5)+1];
        c = res_param[(row*5)+2];
        d = res_param[(row*5)+3];
        e = res_param[(row*5)+4];
    }

	h = MultAddPrime32(x,
			MultAddPrime32(x,
				MultAddPrime32(x,
				MultAddPrime32(x, a, b),
				 c),
			d),
	   e);

	h = (h & p) + (h >> 61);
	if(h >= p) h -= p;
/*
    if(type == 1 && level == 0 && row == 0) {        
        debug[0] = a;
        local_csr_write(local_csr_mailbox_0, debug[0]);

        debug[1] = b;
        local_csr_write(local_csr_mailbox_0, debug[1]);

        debug[2] = c;
        local_csr_write(local_csr_mailbox_0, debug[2]);

        debug[3] = d;
        local_csr_write(local_csr_mailbox_0, debug[3]);

        debug[4] = e;
        local_csr_write(local_csr_mailbox_0, debug[4]);

        debug[5] = x;
        local_csr_write(local_csr_mailbox_0, debug[5]);

        debug[6] = h & 1023;
        local_csr_write(local_csr_mailbox_0, debug[6]);

        local_csr_write(local_csr_mailbox_0, a);
        local_csr_write(local_csr_mailbox_1, b);
        local_csr_write(local_csr_mailbox_2, c);
        local_csr_write(local_csr_mailbox_3, h);
    }
    */
	return h;
}

__forceinline int level_hash(int level, uint32_t x)
{
    int ret = cw32_hash(0, level, 0, x) & 1;
    return ret;
}

__forceinline int index_hash(int level, int row, uint32_t x)
{
    int ret = cw32_hash(1, level, row, x) & 1023;
    return ret;
}

__forceinline int res_hash(int level, int row, uint32_t x)
{
    int ret = cw32_hash(2, level, row, x) & 1;
    return ret;
}

void sketch(int dimension, int level, uint32_t feature, uint32_t elem)
{
    __lmem int row;
    __lmem int index;
    __lmem int res;

    __xread int32_t in_xfer;
    __xwrite int32_t out_xfer;

	for(row=0; row<ROW; row++) {
		index = index_hash(level, row, feature);
        index = level*ROW*WIDTH + row*WIDTH + index;
        index = dimension*WIDTH*ROW*(SAMPLING_LEVEL+1) + index;


		res = res_hash(level, row, feature);
		res = res*2 - 1;
        res = elem * res;

        // 1. sync code
        out_xfer = res;
        mem_add32(&out_xfer, &sketchArray[index], sizeof(out_xfer));
    }
}

__lmem int first = 0;
static void univmon_start(int dimension, uint32_t key, uint32_t packet_len)
{
    __lmem uint32_t rand;
    __lmem uint16_t etherType;
    __lmem uint32_t feature;
    __lmem uint32_t elem;
    __lmem int i;
    __lmem int hash;
    __xwrite int32_t out_xfer = 1;

    if (first == 0) {
        local_csr_write(local_csr_pseudo_random_number,
                        (local_csr_read(local_csr_timestamp_low) & 0xffff) + 1);
        local_csr_read(local_csr_pseudo_random_number);
        first = 1;
    }
    rand = local_csr_read(local_csr_pseudo_random_number);
    feature = rand;
    
//    feature = key;
    elem = 1;
//    elem = packet_len;

    hash = 1;
    for(i=0; i<=SAMPLING_LEVEL; i++) {
        if(i > 0) {
            hash = hash * level_hash(i, feature);
        }

        if(hash == 0) {
//            mem_add32(&out_xfer, &debug[i], sizeof(out_xfer));
            break;
        }

        sketch(dimension, i, feature, elem);
    }
}

int mc_main(void)
{
    int i;
    PKT_META_TYPE struct pkt_meta *pmeta = PKT_META_PTR;
    __mem uint8_t *pktdata = (__mem uint8_t *) pmeta->pkt_buf;

    __xread struct hdr_ethernet rd_eth;
    struct hdr_ethernet buf_eth;

    __xread struct hun_iphdr rd_ip;
    struct hun_iphdr buf_ip;

    mem_read8(&rd_eth, pktdata, sizeof(struct hdr_ethernet));
    buf_eth = rd_eth;

    if(buf_eth.etherType == ETHERTYPE_IPV4) {
        mem_read8(&rd_ip, pktdata+14, sizeof(struct hun_iphdr));
        buf_ip = rd_ip;

        for(i=0; i<DIMENSION; i++) {
            univmon_start(i, buf_ip.daddr, buf_ip.tot_len+14);
        }
    }

    /* packet forwarding */

    if(pmeta->ig_port.type == 0) {
        pmeta->eg_port.type = 1;
        pmeta->eg_port.port = 0;
    }
    else if(pmeta->ig_port.type == 1) {
        pmeta->eg_port.type = 0;
        pmeta->eg_port.port = 0;
    }

    return MC_FORWARD;
}