#include <stdint.h>
#include <nfp.h>
#include <nfp/mem_atomic.h>

/* Netronome FlowEnv MicroEngine library */
#include <nfp/me.h>

/* Netronome FlowEnv bulk memory library */
#include <nfp/mem_bulk.h>
#include "pkt_meta.h"

#define DIMENSION 3

/* ethernet (14B) */
struct hdr_ethernet {
    /* dstAddr [32;16] */
    unsigned int dstAddr_0:32;
    /* dstAddr [16;0] */
    unsigned int dstAddr_1:16;
    /* srcAddr [16;32] */
    unsigned int srcAddr_0:16;
    /* srcAddr [32;0] */
    unsigned int srcAddr_1:32;
    unsigned int etherType:16;
};

struct hun_iphdr 
{ 
	unsigned int ihl:4;
	unsigned int version:4;
	uint8_t tos;
	uint16_t tot_len;
	uint16_t id;
	uint16_t frag_off;
	uint8_t ttl;
	uint8_t protocol;
	uint16_t check;
	uint32_t saddr;
	uint32_t daddr;
};

#define ETHERTYPE_IPV4 0x0800

#define WIDTH 1024
#define ROW 5

#define SAMPLING_LEVEL 16

__export __emem int32_t sketchArray[DIMENSION*WIDTH*ROW*(SAMPLING_LEVEL+1)];

__export __mem uint64_t debug[2000];

__declspec(cls shared) uint64_t level_a[20] = {1336194240, 1078891202, 21879940, 1357841119, 25386003, 1493874153, 1677237226, 904386158, 1307227813, 1610903643, 346419849, 161999441, 234406593, 597878, 30212930, 1469564551, 1157348211};
__declspec(cls shared) uint64_t level_b[20] = {1845201762, 769232884, 1959160474, 951553832, 1469771701, 1825073150, 1931489396, 1826684104, 260379252, 240493913, 1516874301, 321197753, 1652264029, 1180470601, 2116397343, 1228672509, 1688760658};

__declspec(cls shared) uint64_t index_a[100] = {2042630059, 670179352, 1160473557, 2107316268, 1814015083, 2042630059, 670179352, 1160473557, 2107316268, 1814015083, 2042630059, 670179352, 1160473557, 2107316268, 1814015083, 2042630059, 670179352, 1160473557, 2107316268, 1814015083, 2042630059, 670179352, 1160473557, 2107316268, 1814015083, 2042630059, 670179352, 1160473557, 2107316268, 1814015083, 2042630059, 670179352, 1160473557, 2107316268, 1814015083, 2042630059, 670179352, 1160473557, 2107316268, 1814015083, 2042630059, 670179352, 1160473557, 2107316268, 1814015083, 2042630059, 670179352, 1160473557, 2107316268, 1814015083, 2042630059, 670179352, 1160473557, 2107316268, 1814015083, 2042630059, 670179352, 1160473557, 2107316268, 1814015083, 2042630059, 670179352, 1160473557, 2107316268, 1814015083, 2042630059, 670179352, 1160473557, 2107316268, 1814015083, 2042630059, 670179352, 1160473557, 2107316268, 1814015083, 2042630059, 670179352, 1160473557, 2107316268, 1814015083, 2042630059, 670179352, 1160473557, 2107316268, 1814015083};
__declspec(cls shared) uint64_t index_b[100] = {2052146458, 1334232525, 1903556465, 1741247892, 1452047110, 2052146458, 1334232525, 1903556465, 1741247892, 1452047110, 2052146458, 1334232525, 1903556465, 1741247892, 1452047110, 2052146458, 1334232525, 1903556465, 1741247892, 1452047110, 2052146458, 1334232525, 1903556465, 1741247892, 1452047110, 2052146458, 1334232525, 1903556465, 1741247892, 1452047110, 2052146458, 1334232525, 1903556465, 1741247892, 1452047110, 2052146458, 1334232525, 1903556465, 1741247892, 1452047110, 2052146458, 1334232525, 1903556465, 1741247892, 1452047110, 2052146458, 1334232525, 1903556465, 1741247892, 1452047110, 2052146458, 1334232525, 1903556465, 1741247892, 1452047110, 2052146458, 1334232525, 1903556465, 1741247892, 1452047110, 2052146458, 1334232525, 1903556465, 1741247892, 1452047110, 2052146458, 1334232525, 1903556465, 1741247892, 1452047110, 2052146458, 1334232525, 1903556465, 1741247892, 1452047110, 2052146458, 1334232525, 1903556465, 1741247892, 1452047110, 2052146458, 1334232525, 1903556465, 1741247892, 1452047110};

__declspec(cls shared) uint64_t res_a[100] = {570470507, 726927632, 1740912049, 995556346, 492643303, 570470507, 726927632, 1740912049, 995556346, 492643303, 570470507, 726927632, 1740912049, 995556346, 492643303, 570470507, 726927632, 1740912049, 995556346, 492643303, 570470507, 726927632, 1740912049, 995556346, 492643303, 570470507, 726927632, 1740912049, 995556346, 492643303, 570470507, 726927632, 1740912049, 995556346, 492643303, 570470507, 726927632, 1740912049, 995556346, 492643303, 570470507, 726927632, 1740912049, 995556346, 492643303, 570470507, 726927632, 1740912049, 995556346, 492643303, 570470507, 726927632, 1740912049, 995556346, 492643303, 570470507, 726927632, 1740912049, 995556346, 492643303, 570470507, 726927632, 1740912049, 995556346, 492643303, 570470507, 726927632, 1740912049, 995556346, 492643303, 570470507, 726927632, 1740912049, 995556346, 492643303, 570470507, 726927632, 1740912049, 995556346, 492643303, 570470507, 726927632, 1740912049, 995556346, 492643303};
__declspec(cls shared) uint64_t res_b[100] = {1485394327, 1832725924, 595384361, 1556351641, 902294577, 1485394327, 1832725924, 595384361, 1556351641, 902294577, 1485394327, 1832725924, 595384361, 1556351641, 902294577, 1485394327, 1832725924, 595384361, 1556351641, 902294577, 1485394327, 1832725924, 595384361, 1556351641, 902294577, 1485394327, 1832725924, 595384361, 1556351641, 902294577, 1485394327, 1832725924, 595384361, 1556351641, 902294577, 1485394327, 1832725924, 595384361, 1556351641, 902294577, 1485394327, 1832725924, 595384361, 1556351641, 902294577, 1485394327, 1832725924, 595384361, 1556351641, 902294577, 1485394327, 1832725924, 595384361, 1556351641, 902294577, 1485394327, 1832725924, 595384361, 1556351641, 902294577, 1485394327, 1832725924, 595384361, 1556351641, 902294577, 1485394327, 1832725924, 595384361, 1556351641, 902294577, 1485394327, 1832725924, 595384361, 1556351641, 902294577, 1485394327, 1832725924, 595384361, 1556351641, 902294577, 1485394327, 1832725924, 595384361, 1556351641, 902294577};

__declspec(cls shared) uint32_t p = 39916801;

__forceinline int compute_hash(uint32_t x, int range, uint32_t aParam, uint32_t bParam)
{
    return ((aParam * x + bParam) % p) % range;
}

__forceinline int level_hash(int level, uint32_t feature)
{
    return compute_hash(feature, 2, level_a[level], level_b[level]);
}

__forceinline int index_hash(int level, int row, uint32_t feature)
{
    return compute_hash(feature, WIDTH, index_a[level * ROW + row], index_b[level * ROW + row]);
}

__forceinline int res_hash(int level, int row, uint32_t feature)
{
    return compute_hash(feature, 2, res_a[level * ROW + row], res_b[level * ROW + row]);
}

void sketch(int dimension, int level, uint32_t feature, uint32_t elem)
{
    __lmem int row;
    __lmem int index;
    __lmem int res;

    __xread int32_t in_xfer;
    __xwrite int32_t out_xfer;

	for(row=0; row<ROW; row++) {
		index = index_hash(level, row, feature);
        index = level*ROW*WIDTH + row*WIDTH + index;
        index = dimension*WIDTH*ROW*(SAMPLING_LEVEL+1) + index;


		res = res_hash(level, row, feature);
		res = res*2 - 1;
        res = elem * res;

        // 1. sync code
        out_xfer = res;
        mem_add32(&out_xfer, &sketchArray[index], sizeof(out_xfer));
    }
}

__lmem int first = 0;
static void univmon_start(int dimension, uint32_t key, uint32_t packet_len)
{
    __lmem uint32_t rand;
    __lmem uint16_t etherType;
    __lmem uint32_t feature;
    __lmem uint32_t elem;
    __lmem int i;
    __lmem int hash;
    __xwrite int32_t out_xfer = 1;

    if (first == 0) {
        local_csr_write(local_csr_pseudo_random_number,
                        (local_csr_read(local_csr_timestamp_low) & 0xffff) + 1);
        local_csr_read(local_csr_pseudo_random_number);
        first = 1;
    }
    rand = local_csr_read(local_csr_pseudo_random_number);
    feature = rand;

//    feature = key;
    elem = 1;

//    elem = packet_len;

    hash = 1;
    for(i=0; i<=SAMPLING_LEVEL; i++) {
        if(i > 0) {
            hash = hash * level_hash(i, feature);
        }

        if(hash == 0) {
//            mem_add32(&out_xfer, &debug[i], sizeof(out_xfer));
            break;
        }

        sketch(dimension, i, feature, elem);
    }
}

int mc_main(void)
{
    int i;
    PKT_META_TYPE struct pkt_meta *pmeta = PKT_META_PTR;
    __mem uint8_t *pktdata = (__mem uint8_t *) pmeta->pkt_buf;

    __xread struct hdr_ethernet rd_eth;
    struct hdr_ethernet buf_eth;

    __xread struct hun_iphdr rd_ip;
    struct hun_iphdr buf_ip;

    mem_read8(&rd_eth, pktdata, sizeof(struct hdr_ethernet));
    buf_eth = rd_eth;

    if(buf_eth.etherType == ETHERTYPE_IPV4) {
        mem_read8(&rd_ip, pktdata+14, sizeof(struct hun_iphdr));
        buf_ip = rd_ip;

        for(i=0; i<DIMENSION; i++) {
            univmon_start(i, buf_ip.daddr, buf_ip.tot_len+14);
        }
    }

    /* packet forwarding */

    if(pmeta->ig_port.type == 0) {
        pmeta->eg_port.type = 1;
        pmeta->eg_port.port = 0;
    }
    else if(pmeta->ig_port.type == 1) {
        pmeta->eg_port.type = 0;
        pmeta->eg_port.port = 0;
    }

    return MC_FORWARD;
}