#include <stdint.h>
#include <nfp.h>
#include <nfp/mem_atomic.h>


/* Netronome FlowEnv MicroEngine library */
#include <nfp/me.h>

/* Netronome FlowEnv bulk memory library */
#include <nfp/mem_bulk.h>
#include "pkt_meta.h"

/**************************************************************************
* Defines *
**************************************************************************/

/* ethernet (14B) */
struct hdr_ethernet {
    /* dstAddr [32;16] */
    unsigned int dstAddr_0:32;
    /* dstAddr [16;0] */
    unsigned int dstAddr_1:16;
    /* srcAddr [16;32] */
    unsigned int srcAddr_0:16;
    /* srcAddr [32;0] */
    unsigned int srcAddr_1:32;
    unsigned int etherType:16;
};

__export __mem int32_t hun[1000];
__export __mem int16_t hun16[1000] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

__lmem int first = 0;

int mc_main(void)
{
    PKT_META_TYPE struct pkt_meta *pmeta = PKT_META_PTR;
    __xwrite int32_t out_xfer;
    int i;

    out_xfer = 1;
    for(i=0; i<100; i++) {
        mem_add32(&out_xfer, &hun[i], sizeof(out_xfer));
    }
    hun16[10]++;

    if(pmeta->ig_port.type == 0) {
        pmeta->eg_port.type = 1;
        pmeta->eg_port.port = 0;
    }
    else if(pmeta->ig_port.type == 1) {
        pmeta->eg_port.type = 0;
        pmeta->eg_port.port = 0;
    }
    
    return MC_FORWARD;
}