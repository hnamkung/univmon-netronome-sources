#include <stdint.h>
#include <nfp.h>
#include <nfp/mem_atomic.h>

/* Netronome FlowEnv MicroEngine library */
#include <nfp/me.h>

/* Netronome FlowEnv bulk memory library */
#include <nfp/mem_bulk.h>
#include "pkt_meta.h"

//#include "tabulation_hash.h"



/* ethernet (14B) */
struct hdr_ethernet {
    /* dstAddr [32;16] */
    unsigned int dstAddr_0:32;
    /* dstAddr [16;0] */
    unsigned int dstAddr_1:16;
    /* srcAddr [16;32] */
    unsigned int srcAddr_0:16;
    /* srcAddr [32;0] */
    unsigned int srcAddr_1:32;
    unsigned int etherType:16;
};

struct hun_iphdr 
{ 
	unsigned int ihl:4;
	unsigned int version:4;
	uint8_t tos;
	uint16_t tot_len;
	uint16_t id;
	uint16_t frag_off;
	uint8_t ttl;
	uint8_t protocol;
	uint16_t check;
	uint32_t saddr;
	uint32_t daddr;
};

#define ETHERTYPE_IPV4 0x0800

#define WIDTH 1024
#define ROW 5

#define SAMPLING_LEVEL 16

__export __emem uint64_t level_t0[256 * (SAMPLING_LEVEL+1)];
__export __emem uint64_t level_t1[256 * (SAMPLING_LEVEL+1)];
__export __emem uint64_t level_t2[256 * (SAMPLING_LEVEL+1)];
__export __emem uint64_t level_t3[256 * (SAMPLING_LEVEL+1)];
__export __emem uint32_t level_t4[1024 * (SAMPLING_LEVEL+1)];
__export __emem uint32_t level_t5[1024 * (SAMPLING_LEVEL+1)];
__export __emem uint32_t level_t6[4096 * (SAMPLING_LEVEL+1)];

__export __imem uint64_t index_t0[256 * ROW];
__export __imem uint64_t index_t1[256 * ROW];
__export __imem uint64_t index_t2[256 * ROW];
__export __imem uint64_t index_t3[256 * ROW];
__export __imem uint32_t index_t4[1024 * ROW];
__export __imem uint32_t index_t5[1024 * ROW];
__export __imem uint32_t index_t6[4096 * ROW];

__export __emem uint64_t res_t0[256 * ROW];
__export __emem uint64_t res_t1[256 * ROW];
__export __emem uint64_t res_t2[256 * ROW];
__export __emem uint64_t res_t3[256 * ROW];
__export __emem uint32_t res_t4[1024 * ROW];
__export __emem uint32_t res_t5[1024 * ROW];
__export __emem uint32_t res_t6[4096 * ROW];

//__export __mem int32_t sketchArray[153000];
__export __imem int32_t sketchArray[WIDTH*ROW*(SAMPLING_LEVEL+1)];

__export __mem uint32_t debug[2000];

int level_hash(int level, uint32_t x)
{
	uint8_t x0, x1, x2, x3;
	uint32_t a00, a01, a10, a11, a20, a21, a30, a31;
	uint32_t c;
    uint32_t start_index;
    uint32_t start_index2;
    uint32_t start_index3;
    int ret;

    start_index = level * 256;
    start_index2 = level * 1024;
    start_index3 = level * 4096;

	x0 = (x >> 24) & 0xff;
	x1 = (x >> 16) & 0xff;
	x2 = (x >> 8) & 0xff;
	x3 = x & 0xff;

	a00 = (level_t0[start_index + x0] >> 32) & 0xffffffff;
	a01 = level_t0[start_index + x0] & 0xffffffff;

	a10 = (level_t1[start_index + x1] >> 32) & 0xffffffff;
	a11 = level_t1[start_index + x1] & 0xffffffff;

	a20 = (level_t2[start_index + x2] >> 32) & 0xffffffff;
	a21 = level_t2[start_index + x2] & 0xffffffff;

	a30 = (level_t3[start_index + x3] >> 32) & 0xffffffff;
	a31 = level_t3[start_index + x3] & 0xffffffff;

	c = a01 + a11 + a21 + a31;
    ret = (a00 ^ a10 ^ a20 ^ a30 ^ level_t4[start_index2 + (c&1023)] ^ level_t5[start_index2 + ((c>>10)&1023)] ^ level_t6[start_index3 + (c>>20)]);
	return ret & 1;
}

int index_hash(int level, int row, uint32_t x)
{
	uint8_t x0, x1, x2, x3;
	uint32_t a00, a01, a10, a11, a20, a21, a30, a31;
	uint32_t c;
    uint32_t start_index;
    uint32_t start_index2;
    uint32_t start_index3;
    int ret;

    start_index = (row) * 256;
    start_index2 = (row) * 1024;
    start_index3 = (row) * 4096;

	x0 = (x >> 24) & 0xff;
	x1 = (x >> 16) & 0xff;
	x2 = (x >> 8) & 0xff;
	x3 = x & 0xff;

	a00 = (index_t0[start_index + x0] >> 32) & 0xffffffff;
	a01 = index_t0[start_index + x0] & 0xffffffff;

	a10 = (index_t1[start_index + x1] >> 32) & 0xffffffff;
	a11 = index_t1[start_index + x1] & 0xffffffff;

	a20 = (index_t2[start_index + x2] >> 32) & 0xffffffff;
	a21 = index_t2[start_index + x2] & 0xffffffff;

	a30 = (index_t3[start_index + x3] >> 32) & 0xffffffff;
	a31 = index_t3[start_index + x3] & 0xffffffff;

	c = a01 + a11 + a21 + a31;
    ret = (a00 ^ a10 ^ a20 ^ a30 ^ index_t4[start_index2 + (c&1023)] ^ index_t5[start_index2 + ((c>>10)&1023)] ^ index_t6[start_index3 + (c>>20)]);
	return ret & 1023;
}

int res_hash(int level, int row, uint32_t x)
{
	uint8_t x0, x1, x2, x3;
	uint32_t a00, a01, a10, a11, a20, a21, a30, a31;
	uint32_t c;
    uint32_t start_index;
    uint32_t start_index2;
    uint32_t start_index3;
    int ret;

    start_index = (row) * 256;
    start_index2 = (row) * 1024;
    start_index3 = (row) * 4096;

	x0 = (x >> 24) & 0xff;
	x1 = (x >> 16) & 0xff;
	x2 = (x >> 8) & 0xff;
	x3 = x & 0xff;

	a00 = (res_t0[start_index + x0] >> 32) & 0xffffffff;
	a01 = res_t0[start_index + x0] & 0xffffffff;

	a10 = (res_t1[start_index + x1] >> 32) & 0xffffffff;
	a11 = res_t1[start_index + x1] & 0xffffffff;

	a20 = (res_t2[start_index + x2] >> 32) & 0xffffffff;
	a21 = res_t2[start_index + x2] & 0xffffffff;

	a30 = (res_t3[start_index + x3] >> 32) & 0xffffffff;
	a31 = res_t3[start_index + x3] & 0xffffffff;

	c = a01 + a11 + a21 + a31;
    ret = (a00 ^ a10 ^ a20 ^ a30 ^ res_t4[start_index2 + (c&1023)] ^ res_t5[start_index2 + ((c>>10)&1023)] ^ res_t6[start_index3 + (c>>20)]);
	return ret & 1;
}


void sketch(int level, uint32_t feature, uint32_t elem)
{
    __lmem int row;
    __lmem int index;
    __lmem int res;

    __xread int32_t in_xfer;
    __xwrite int32_t out_xfer;

	for(row=0; row<ROW; row++) {
		index = index_hash(level, row, feature);
        index = level*ROW*WIDTH + row*WIDTH + index;

		res = res_hash(level, row, feature);
		res = res*2 - 1;
        res = elem * res;

//        local_csr_write(local_csr_mailbox_0, index);
//        local_csr_write(local_csr_mailbox_1, res);

//        res = 1;
//        index = 10;

        // 1. sync code
        out_xfer = res;
        mem_add32(&out_xfer, &sketchArray[index], sizeof(out_xfer));

        // 2. no sync code
//        mem_read32(&in_xfer, &sketchArray[index], sizeof(in_xfer));
//        res = in_xfer + res;
//        out_xfer = res;
//        mem_write32(&out_xfer, &sketchArray[index], sizeof(out_xfer));

        // 3. no sync code for cls
//        sketchArray[index] = sketchArray[index] + res;

    }
}

__lmem int first = 0;
static void univmon_start(uint32_t key, uint32_t packet_len)
{
    __lmem uint32_t rand;
    __lmem uint16_t etherType;
    __lmem uint32_t feature;
    __lmem uint32_t elem;
    __lmem int i;
    __lmem int hash;
    __xwrite int32_t out_xfer = 1;

    /*
    if (first == 0) {
        local_csr_write(local_csr_pseudo_random_number,
                        (local_csr_read(local_csr_timestamp_low) & 0xffff) + 1);
        local_csr_read(local_csr_pseudo_random_number);
        first = 1;
    }
    rand = local_csr_read(local_csr_pseudo_random_number);    
    feature = rand;
    */
//    elem = 1;
    
    feature = key;
    elem = packet_len;

    hash = 1;
    for(i=0; i<=SAMPLING_LEVEL; i++) {
        if(i > 0) {
            hash = hash * level_hash(i, feature);
        }

        if(hash == 0) {
            mem_add32(&out_xfer, &debug[i], sizeof(out_xfer));
            break;
        }

        sketch(i, feature, elem);
    }
}

int mc_main(void)
{
    PKT_META_TYPE struct pkt_meta *pmeta = PKT_META_PTR;
    __mem uint8_t *pktdata = (__mem uint8_t *) pmeta->pkt_buf;

    __xread struct hdr_ethernet rd_eth;
    struct hdr_ethernet buf_eth;

    __xread struct hun_iphdr rd_ip;
    struct hun_iphdr buf_ip;

    mem_read8(&rd_eth, pktdata, sizeof(struct hdr_ethernet));
    buf_eth = rd_eth;

    if(buf_eth.etherType == ETHERTYPE_IPV4) {
        mem_read8(&rd_ip, pktdata+14, sizeof(struct hun_iphdr));
        buf_ip = rd_ip;
        univmon_start(buf_ip.daddr, buf_ip.tot_len+14);
    }
    /* packet forwarding */

    if(pmeta->ig_port.type == 0) {
        pmeta->eg_port.type = 1;
        pmeta->eg_port.port = 0;
    }
    else if(pmeta->ig_port.type == 1) {
        pmeta->eg_port.type = 0;
        pmeta->eg_port.port = 0;
    }

    return MC_FORWARD;
}