#include <stdint.h>
#include <nfp.h>
#include <nfp/mem_atomic.h>

/* Netronome FlowEnv MicroEngine library */
#include <nfp/me.h>

/* Netronome FlowEnv bulk memory library */
#include <nfp/mem_bulk.h>
#include "pkt_meta.h"

#define DIMENSION 3

/* ethernet (14B) */
struct hdr_ethernet {
    /* dstAddr [32;16] */
    unsigned int dstAddr_0:32;
    /* dstAddr [16;0] */
    unsigned int dstAddr_1:16;
    /* srcAddr [16;32] */
    unsigned int srcAddr_0:16;
    /* srcAddr [32;0] */
    unsigned int srcAddr_1:32;
    unsigned int etherType:16;
};

struct hun_iphdr 
{ 
	unsigned int ihl:4;
	unsigned int version:4;
	uint8_t tos;
	uint16_t tot_len;
	uint16_t id;
	uint16_t frag_off;
	uint8_t ttl;
	uint8_t protocol;
	uint16_t check;
	uint32_t saddr;
	uint32_t daddr;
};

#define ETHERTYPE_IPV4 0x0800

#define WIDTH 1024
#define ROW 5

#define SAMPLING_LEVEL 16

__export __emem int32_t sketchArray[DIMENSION*WIDTH*ROW*(SAMPLING_LEVEL+1)];

__export __mem uint64_t debug[2000];

__declspec(emem export scope(global)) uint16_t level_t0[65536 * (SAMPLING_LEVEL+1)];
__declspec(emem export scope(global)) uint16_t level_t1[65536 * (SAMPLING_LEVEL+1)];
__declspec(emem export scope(global)) uint16_t level_t2[65536 * 2 * (SAMPLING_LEVEL+1)];

__declspec(emem export scope(global)) uint16_t index_t0[65536 * ROW];
__declspec(emem export scope(global)) uint16_t index_t1[65536 * ROW];
__declspec(emem export scope(global)) uint16_t index_t2[65536 * 2 * ROW];

__declspec(emem export scope(global)) uint16_t res_t0[65536 * ROW];
__declspec(emem export scope(global)) uint16_t res_t1[65536 * ROW];
__declspec(emem export scope(global)) uint16_t res_t2[65536 * 2 * ROW];

__forceinline int short32_hash(int type, int level, int row, uint32_t x)
{
	uint32_t x0, x1, x2;
    uint16_t t0, t1, t2;

    uint32_t start_index;

    t0 = t1 = t2 = 0;

	x0 = x & 65535;
	x1 = x >> 16;
	x2 = x0 + x1;

    if(type == 0) { // level hash
        start_index = (65536 * level);
        t0 = level_t0[start_index + x0];
        t1 = level_t1[start_index + x1];
        t2 = level_t2[(start_index*2) + x2];
    }

    if(type == 1) { // index hash
        start_index = (65536 * row);
        t0 = index_t0[start_index + x0];
        t1 = index_t1[start_index + x1];
        t2 = index_t2[(start_index*2) + x2];
    }

    if(type == 2) { // res hash
        start_index = (65536 * row);
        t0 = res_t0[start_index + x0];
        t1 = res_t1[start_index + x1];
        t2 = res_t2[(start_index*2) + x2];
    }

	return t0 ^ t1 ^ t2;
}

__forceinline int level_hash(int level, uint32_t x)
{
    int ret = short32_hash(0, level, 0, x) & 1;
    return ret;
}

__forceinline int index_hash(int level, int row, uint32_t x)
{
    int ret = short32_hash(1, level, row, x) & 1023;
    return ret;
}

__forceinline int res_hash(int level, int row, uint32_t x)
{
    int ret = short32_hash(2, level, row, x) & 1;
    return ret;
}

void sketch(int dimension, int level, uint32_t feature, uint32_t elem)
{
    __lmem int row;
    __lmem int index;
    __lmem int res;

    __xread int32_t in_xfer;
    __xwrite int32_t out_xfer;

	for(row=0; row<ROW; row++) {
		index = index_hash(level, row, feature);
        index = level*ROW*WIDTH + row*WIDTH + index;
        index = dimension*WIDTH*ROW*(SAMPLING_LEVEL+1) + index;


		res = res_hash(level, row, feature);
		res = res*2 - 1;
        res = elem * res;

        // 1. sync code
        out_xfer = res;
        mem_add32(&out_xfer, &sketchArray[index], sizeof(out_xfer));
    }
}

__lmem int first = 0;
static void univmon_start(int dimension, uint32_t key, uint32_t packet_len)
{
    __lmem uint32_t rand;
    __lmem uint16_t etherType;
    __lmem uint32_t feature;
    __lmem uint32_t elem;
    __lmem int i;
    __lmem int hash;
    __xwrite int32_t out_xfer = 1;

    if (first == 0) {
        local_csr_write(local_csr_pseudo_random_number,
                        (local_csr_read(local_csr_timestamp_low) & 0xffff) + 1);
        local_csr_read(local_csr_pseudo_random_number);
        first = 1;
    }
    rand = local_csr_read(local_csr_pseudo_random_number);
    feature = rand;
    
//    feature = key;
    elem = 1;
//    elem = packet_len;

    hash = 1;
    for(i=0; i<=SAMPLING_LEVEL; i++) {
        if(i > 0) {
            hash = hash * level_hash(i, feature);
        }

        if(hash == 0) {
//            mem_add32(&out_xfer, &debug[i], sizeof(out_xfer));
            break;
        }

        sketch(dimension, i, feature, elem);
    }
}

int mc_main(void)
{
    int i;
    PKT_META_TYPE struct pkt_meta *pmeta = PKT_META_PTR;
    __mem uint8_t *pktdata = (__mem uint8_t *) pmeta->pkt_buf;

    __xread struct hdr_ethernet rd_eth;
    struct hdr_ethernet buf_eth;

    __xread struct hun_iphdr rd_ip;
    struct hun_iphdr buf_ip;

    mem_read8(&rd_eth, pktdata, sizeof(struct hdr_ethernet));
    buf_eth = rd_eth;

    if(buf_eth.etherType == ETHERTYPE_IPV4) {
        mem_read8(&rd_ip, pktdata+14, sizeof(struct hun_iphdr));
        buf_ip = rd_ip;

        for(i=0; i<DIMENSION; i++) {
            univmon_start(i, buf_ip.daddr, buf_ip.tot_len+14);
        }
    }

    /* packet forwarding */

    if(pmeta->ig_port.type == 0) {
        pmeta->eg_port.type = 1;
        pmeta->eg_port.port = 0;
    }
    else if(pmeta->ig_port.type == 1) {
        pmeta->eg_port.type = 0;
        pmeta->eg_port.port = 0;
    }

    return MC_FORWARD;
}