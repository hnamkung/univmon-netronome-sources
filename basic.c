#include <stdint.h>
#include <nfp.h>

/* Netronome FlowEnv MicroEngine library */
#include <nfp/me.h>

/* Netronome FlowEnv bulk memory library */
#include <nfp/mem_bulk.h>
#include "pkt_meta.h"

/**************************************************************************
* Defines *
**************************************************************************/

/* ethernet (14B) */
struct hdr_ethernet {
    /* dstAddr [32;16] */
    unsigned int dstAddr_0:32;
    /* dstAddr [16;0] */
    unsigned int dstAddr_1:16;
    /* srcAddr [16;32] */
    unsigned int srcAddr_0:16;
    /* srcAddr [32;0] */
    unsigned int srcAddr_1:32;
    unsigned int etherType:16;
};

__mem int32_t hun[1000];

__lmem int first = 0;

int mc_main(void)
{
    PKT_META_TYPE struct pkt_meta *pmeta = PKT_META_PTR;
    __lmem uint32_t rand;
    __lmem uint32_t rand_10000;
    int feature;
    int step1, step2, step3, step4, step5;

    if (first == 0) {
        local_csr_write(local_csr_pseudo_random_number,
                        (local_csr_read(local_csr_timestamp_low) & 0xffff) + 1);
        local_csr_read(local_csr_pseudo_random_number);
        first = 1;
    }
    rand = local_csr_read(local_csr_pseudo_random_number);
    rand_10000 = rand % 10000;

    hun[0] = rand_10000;

    feature = rand_10000;


    step1 = (36527972 * feature + 22081161);
    hun[1] += step1;

    step2 = step1 % 39916801;
    hun[2] += step2;

    step3 = step2 % 1800;
    hun[3] += step3;

    step4 = step3 / 55;
    hun[4] += step4;

    step5 = step4 * 55;
    hun[5] += step5;


    /* reflect back */
    pmeta->eg_port.type = pmeta->ig_port.type;
    pmeta->eg_port.port = pmeta->ig_port.port;
    
    return MC_FORWARD;
}