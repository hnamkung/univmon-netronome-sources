#include <stdint.h>
#include <nfp.h>
#include <nfp/mem_atomic.h>

/* Netronome FlowEnv MicroEngine library */
#include <nfp/me.h>

/* Netronome FlowEnv bulk memory library */
#include <nfp/mem_bulk.h>
#include "pkt_meta.h"



/* ethernet (14B) */
struct hdr_ethernet {
    /* dstAddr [32;16] */
    unsigned int dstAddr_0:32;
    /* dstAddr [16;0] */
    unsigned int dstAddr_1:16;
    /* srcAddr [16;32] */
    unsigned int srcAddr_0:16;
    /* srcAddr [32;0] */
    unsigned int srcAddr_1:32;
    unsigned int etherType:16;
};
#define ETHERTYPE_IPV4 0x0800

__declspec(cls shared) int32_t level_a[20] = {36527972, 19007741, 30876446, 151599, 11655449, 24499139, 19903131, 31099797, 1726691, 17672459, 13229126, 18055639, 10160899, 38328824, 20776572, 36886637, 19193361};
__declspec(cls shared) int32_t level_b[20] = {22081161, 313669, 32688101, 26792318, 11262444, 27527885, 36517321, 3111918, 19446319, 10838240, 23055481, 11497699, 2346812, 24280330, 384947, 4878125, 39696388};

__declspec(cls shared) int32_t index_a[100] = {25085448, 26371706, 26306928, 11082950, 16257233, 24249164, 23913393, 22322291, 12232351, 28599341, 12863766, 21773806, 20090795, 24452341, 27809432, 1868185, 38536760, 28591158, 20251234, 17790482, 2760755, 35957295, 21302287, 14591986, 6160141, 18328736, 20803970, 3183546, 26331909, 19061217, 1735628, 172089, 14804029, 29473911, 10600365, 23490381, 21727778, 25686083, 31336961, 38602110, 25353970, 34760911, 23254117, 33826242, 21570841, 11331328, 8824534, 22331460, 25696282, 14212368, 2170654, 16061792, 18467312, 15210904, 31901457, 24344927, 13584611, 14092110, 22096063, 27971323, 12343060, 17672426, 31041797, 14549970, 28337825, 6303500, 5461407, 334334, 29090866, 1697779, 24714480, 6900730, 23914598, 39361342, 28430051, 32772731, 5666631, 7997095, 8401327, 9733658, 37400920, 23816223, 38499765, 5711703, 31908845};
__declspec(cls shared) int32_t index_b[100] = {7977416, 26521849, 23928002, 5650086, 27256962, 25567821, 21372645, 13901931, 16414653, 4162211, 11940106, 21479132, 17294823, 22293892, 26994091, 37708534, 25798880, 35688053, 25984157, 5632978, 21509546, 21936907, 39170800, 32566126, 34335890, 11290146, 17407470, 30474195, 22843448, 32308258, 24745825, 26679090, 22912091, 4771631, 35938117, 18449300, 35465525, 5467154, 8122148, 29617392, 24472611, 7470620, 2044408, 21841775, 157755, 17682032, 33950155, 37875779, 3453372, 30342338, 7866920, 31787020, 28179227, 13632985, 17746559, 31374862, 9453629, 4735466, 3513314, 24446624, 30183506, 27048920, 19216283, 33512553, 15204124, 7479030, 23035105, 14317370, 23782476, 8518427, 586577, 3056333, 20476350, 1361315, 22888486, 24025638, 26277431, 35159039, 28385493, 18937297, 30975576, 16436357, 27294132, 18008324, 7769053};

__declspec(cls shared) int32_t res_a[100] = {12355415, 36854215, 30409442, 34998215, 34669208, 22054142, 16433352, 39006519, 7626789, 764781, 35284908, 38075273, 30532847, 23354441, 15184745, 3754288, 21421882, 19496302, 34268969, 726036, 36698570, 11745793, 31276038, 14716055, 36476544, 15502287, 31937398, 6683883, 36145629, 8225115, 17873898, 14215589, 25502357, 2000741, 10192398, 9240002, 29361457, 32416340, 32727436, 13508414, 6359381, 35262511, 5375655, 10888454, 10505807, 2674128, 16392022, 17731509, 3449335, 18516581, 12923484, 12778440, 12667559, 33164414, 34580040, 2449919, 23390509, 3319300, 15978644, 24444614, 3034758, 25984317, 16022781, 21151192, 26441588, 36088147, 39109781, 31031384, 26217765, 33186952, 19863708, 26127780, 8941686, 21457871, 17549433, 33422141, 23302798, 27235301, 13640761, 30996034, 25900557, 24535220, 10960931, 30739777, 22806246};
__declspec(cls shared) int32_t res_b[100] = {17101256, 17795405, 39054780, 38338085, 14949931, 18438298, 7573374, 6049234, 28137403, 22815689, 31143218, 23271398, 9561289, 32467632, 10682824, 30709524, 23473481, 26800098, 16009886, 29443260, 5568991, 31238353, 39156756, 24624182, 14751657, 18825185, 281340, 7797622, 20726610, 36126898, 34670525, 29798333, 26075283, 29883605, 24225171, 6056924, 30127653, 1616950, 31928823, 8151042, 11453931, 10963908, 30564488, 7647453, 16582173, 2939725, 5652184, 17129976, 3038619, 35391848, 33477005, 18876380, 21362693, 23691745, 38879357, 23102402, 8382631, 7944962, 31135903, 31946057, 35162423, 39326181, 39316161, 4083899, 2157881, 15999332, 24420561, 16254088, 31176152, 10071412, 15607916, 24243616, 344825, 4446906, 10790303, 28178230, 39669213, 29498658, 32712164, 39023243, 4507026, 37569896, 20524038, 558176, 27410321};

__declspec(cls shared) uint32_t p = 39916801;

#define WIDTH 1800
#define ROW 5

#define SAMPLING_LEVEL 16

__export __mem int32_t sketchArray[153000];

int compute_hash(uint32_t x, int range, uint32_t aParam, uint32_t bParam)
{
    return ((aParam * x + bParam) % p) % range;
}

int level_hash(int level, uint32_t feature)
{
    return compute_hash(feature, 2, level_a[level], level_b[level]);
}

int index_hash(int level, int row, uint32_t feature)
{
    return compute_hash(feature, WIDTH, index_a[level * ROW + row], index_b[level * ROW + row]);
}

int res_hash(int level, int row, uint32_t feature)
{
    return compute_hash(feature, 2, res_a[level * ROW + row], res_b[level * ROW + row]);
}


void sketch(int level, uint32_t feature, uint32_t elem)
{
    __lmem int row;
    __lmem int index;
    __lmem int res;

    __xread int32_t in_xfer;
    __xwrite int32_t out_xfer;

	for(row=0; row<ROW; row++) {
		index = index_hash(level, row, feature);
        index = level*ROW*WIDTH + row*WIDTH + index;

		res = res_hash(level, row, feature);
		res = res*2 - 1;
        res = elem * res;

//        local_csr_write(local_csr_mailbox_0, index);
//        local_csr_write(local_csr_mailbox_1, res);

//        res = 1;
//        index = 10;

        // 1. sync code
        out_xfer = res;
        mem_add32(&out_xfer, &sketchArray[index], sizeof(out_xfer));

        // 2. no sync code
//        mem_read32(&in_xfer, &sketchArray[index], sizeof(in_xfer));
//        res = in_xfer + res;
//        out_xfer = res;
//        mem_write32(&out_xfer, &sketchArray[index], sizeof(out_xfer));

        // 3. no sync code for cls
//        sketchArray[index] = sketchArray[index] + res;

    }
}

__lmem int first = 0;
static void univmon_start()
{
    __lmem uint32_t rand;
    __lmem uint32_t rand_10000;
    __lmem uint16_t etherType;
    __lmem uint32_t feature;
    __lmem uint32_t elem;
    __lmem int i;
    __lmem int hash;

    if (first == 0) {
        local_csr_write(local_csr_pseudo_random_number,
                        (local_csr_read(local_csr_timestamp_low) & 0xffff) + 1);
        local_csr_read(local_csr_pseudo_random_number);
        first = 1;
    }
    rand = local_csr_read(local_csr_pseudo_random_number);
    rand_10000 = rand % 10000;

    feature = rand_10000;

    elem = 1;
    
    hash = 1;
    for(i=0; i<=SAMPLING_LEVEL; i++) {
        if(i > 0) {
            hash = hash * level_hash(i, feature);
        }

        if(hash == 0)
            break;

        sketch(i, feature, elem);
    }
}

int mc_main(void)
{
    int i;


    PKT_META_TYPE struct pkt_meta *pmeta = PKT_META_PTR;

    __mem uint8_t *pktdata = (__mem uint8_t *) pmeta->pkt_buf;

    __xread struct hdr_ethernet rd_eth;
    __xwrite struct hdr_ethernet wr_eth;
    struct hdr_ethernet buf_eth;

//    if(__ctx() != 4) {
//        return MC_DROP;
//    }

    mem_read8(&rd_eth, pktdata, sizeof(struct hdr_ethernet));

    buf_eth = rd_eth;

    if(buf_eth.etherType == ETHERTYPE_IPV4) {
        univmon_start();
    }

    /* packet forwarding */

    if(pmeta->ig_port.type == 0) {
        pmeta->eg_port.type = 1;
        pmeta->eg_port.port = 0;
    }
    else if(pmeta->ig_port.type == 1) {
        pmeta->eg_port.type = 0;
        pmeta->eg_port.port = 0;
    }

    return MC_FORWARD;
}