#include <stdint.h>
#include <nfp.h>
#include <std/hash.h>


/* Netronome FlowEnv MicroEngine library */
#include <nfp/me.h>


/* Netronome FlowEnv bulk memory library */
#include <nfp/mem_bulk.h>
#include "pkt_meta.h"


/**************************************************************************
* Defines *
**************************************************************************/


/* ethernet (14B) */
struct hdr_ethernet {
    /* dstAddr [32;16] */
    unsigned int dstAddr_0:32;
    /* dstAddr [16;0] */
    unsigned int dstAddr_1:16;
    /* srcAddr [16;32] */
    unsigned int srcAddr_0:16;
    /* srcAddr [32;0] */
    unsigned int srcAddr_1:32;
    unsigned int etherType:16;
};

__lmem int32_t count;
__lmem int32_t key[50];
__lmem int32_t hash_array[50];


int mc_main(void)
{
    PKT_META_TYPE struct pkt_meta *pmeta = PKT_META_PTR;
    __gpr int32_t k;
    __gpr int32_t hash_val;
    int thread_id;

    thread_id = __ctx();
    if(thread_id == 4) {
        k = __ME()*10+thread_id;
        hash_val = hash_me_crc32(&k, 4, 0);
    
        count++;
        if(count >= 50)
            count = 0;
        key[count] = __ME()*10+thread_id;
        hash_array[count] = hash_val;


        /*
        if(thread_id == 4) {
            local_csr_write(local_csr_mailbox0, hash_val);
        }
        else if(thread_id == 5) {
            local_csr_write(local_csr_mailbox1, hash_val);
        }
        else if(thread_id == 6) {
            local_csr_write(local_csr_mailbox2, hash_val);
        }
        else if(thread_id == 7) {
            local_csr_write(local_csr_mailbox3, hash_val);
        }
        */
    }


    if(pmeta->ig_port.type == 0) {
        pmeta->eg_port.type = 1;
        pmeta->eg_port.port = 0;
    }
    else if(pmeta->ig_port.type == 1) {
        pmeta->eg_port.type = 0;
        pmeta->eg_port.port = 0;
    }
    
    return MC_FORWARD;
}